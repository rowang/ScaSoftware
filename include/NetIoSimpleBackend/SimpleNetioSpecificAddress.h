/*
 * SimpleNetioSpecificAddress.h
 *
 *  Created on: Mar 14, 2017
 *      Author: pnikiel
 */

#ifndef INCLUDE_NETIOSIMPLEBACKEND_SIMPLENETIOSPECIFICADDRESS_H_
#define INCLUDE_NETIOSIMPLEBACKEND_SIMPLENETIOSPECIFICADDRESS_H_

#include <string>

namespace NetIoSimpleBackend
{

class SpecificAddress
{
public:
	//! Constructs the specific address from its string representation. It could be in direct form or as the mapper.
	SpecificAddress (const std::string& stringAddress);

	//! string representation of this specific address, note it might differ from requested specific address in case dynamic traslation was used
	std::string toString() const { return m_felixHostname+"/"; }

	std::string felixHostname() const { return m_felixHostname; }
	unsigned int portToSca() const { return m_portToSca; }
	unsigned int portFromSca() const { return m_portFromSca; }
	unsigned int elinkIdTx() const { return m_elinkIdTx; }
	unsigned int elinkIdRx() const { return m_elinkIdRx; }

private:
	std::string m_felixHostname;
	unsigned int m_portToSca;
	unsigned int m_portFromSca;
	uint32_t m_elinkIdTx;
	uint32_t m_elinkIdRx;
};

}



#endif /* INCLUDE_NETIOSIMPLEBACKEND_SIMPLENETIOSPECIFICADDRESS_H_ */
