#ifndef FLXDEFS_H
#define FLXDEFS_H

// GBT-SCA device identifiers (Channel byte)
#define SCA_DEV_CONFIG         0x00
#define SCA_DEV_JTAG           0x13

// SCA_DEV_CONFIG commands
#define SCA_CONFIG_WR_A        0x00
#define SCA_CONFIG_RD_A        0x01
#define SCA_CONFIG_WR_B        0x02
#define SCA_CONFIG_RD_B        0x03
#define SCA_CONFIG_WR_C        0x04
#define SCA_CONFIG_RD_C        0x05
#define SCA_CONFIG_WR_D        0x06
#define SCA_CONFIG_RD_D        0x07


// SCA_DEV_JTAG commands
#define SCA_JTAG_WR_CTRL       0x80
#define SCA_JTAG_RD_CTRL       0x81
#define SCA_JTAG_WR_FREQ       0x90
#define SCA_JTAG_RD_FREQ       0x91
#define SCA_JTAG_WR_TDO0       0x00
#define SCA_JTAG_WR_TDO1       0x10
#define SCA_JTAG_WR_TDO2       0x20
#define SCA_JTAG_WR_TDO3       0x30
#define SCA_JTAG_RD_TDI0       0x01
#define SCA_JTAG_RD_TDI1       0x11
#define SCA_JTAG_RD_TDI2       0x21
#define SCA_JTAG_RD_TDI3       0x31
#define SCA_JTAG_WR_TMS0       0x40
#define SCA_JTAG_WR_TMS1       0x50
#define SCA_JTAG_WR_TMS2       0x60
#define SCA_JTAG_WR_TMS3       0x70
#define SCA_JTAG_RD_TMS0       0x41
#define SCA_JTAG_RD_TMS1       0x51
#define SCA_JTAG_RD_TMS2       0x61
#define SCA_JTAG_RD_TMS3       0x71
#define SCA_JTAG_ARESET        0xC0
#define SCA_JTAG_GO            0xA2
#define SCA_JTAG_GO_MANUAL     0xB0


#endif // FLXDEFS_H
