/*
 * Xilinx7SeriesJtagProgrammer.h
 *
 *  Created on: 18 Jun 2018
 *      Author: pnikiel
 */

#ifndef INCLUDE_HENKSJTAG_XILINX7SERIESJTAGPROGRAMMER_H_
#define INCLUDE_HENKSJTAG_XILINX7SERIESJTAGPROGRAMMER_H_

#include <Sca/Sca.h>
#include <HenksJtag/JtagPort.h>
#include <HenksJtag/FlxUpload.h>

namespace HenksJtag
{

class BitfileReader;

class Xilinx7SeriesJtagProgrammer
{
public:
    Xilinx7SeriesJtagProgrammer(Sca::Sca& sca, unsigned int freqMhz=20.0, unsigned int maxGroupSize=240);
    virtual ~Xilinx7SeriesJtagProgrammer();

    bool programFromBitFile (const std::string& bitfile,
                             bool               reverseBytes);
    bool programFromBlob (const std::string& bloc,
                          bool                reverseBytes);

    bool programFromBitFileReader (BitfileReader& reader);

    uint32_t readId ();

    static std::string idCodeToString (uint32_t id);

private:
    Sca::Sca& m_sca;
    HenksJtag::FlxUpload m_flxUpload;
    HenksJtag::JtagPort jtag;
    unsigned int m_freqMhz;

};

} /* namespace HenksJtag */

#endif /* INCLUDE_HENKSJTAG_XILINX7SERIESJTAGPROGRAMMER_H_ */
