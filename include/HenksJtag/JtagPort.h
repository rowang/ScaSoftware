#ifndef JTAGPORT_H
#define JTAGPORT_H

#include <HenksJtag/FlxUpload.h>

namespace HenksJtag
{

class JtagPort
{
 public:

  typedef enum {
    SELECT_DR_SCAN = 0,
    CAPTURE_DR,
    SHIFT_DR,
    EXIT1_DR,
    PAUSE_DR,
    EXIT2_DR,
    UPDATE_DR,

    TEST_LOGIC_RESET,
    RUN_TEST_IDLE,

    SELECT_IR_SCAN,
    CAPTURE_IR,
    SHIFT_IR,
    EXIT1_IR,
    PAUSE_IR,
    EXIT2_IR,
    UPDATE_IR
  } state_t;

  JtagPort( FlxUpload &fup, int elinknr );
  ~JtagPort();

 public:
  void    enable   ( int freq_mhz );
  void    gotoState( state_t s );
  state_t state    ( )           { return _state; }
  void    setIrLen ( int len )   { _irLen = len; }
  void    shiftIr  ( int instr );
  void    shiftDr  ( int nbits, u8 *tdo = 0, bool get_tdi = false );
  void    shift    ( int nbits, bool get_tdi = false );

  void    resetTrid( )           { _trid = 1; }
  void    showProgress( bool b ) { _progress = b; }

 private:
  int     setState ( state_t s, uint32_t *tms );
  void    nextState( int tms );

  void    addResetTdoTms( );

  void    mapInt2Bytes( uint32_t i, u8 *bytes );

  void    displayPercentage( int *percentage,
                             int  bytes_done,
                             int  bytes_todo );
 private:
  // For JTAG
  state_t    _state;
  int        _irLen;    // One device in the JTAG chain for the time being..
  bool       _progress; // Whether or not to display progress in shiftDr()

  // For GBT-SCA via FLX
  int        _elinknr;
  int        _trid;
  uint32_t   _ctrlReg;
  int        _goDelay;
  FlxUpload &_fup;
};

}

#endif // JTAGPORT_H
