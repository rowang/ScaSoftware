/* Henk Boterenbrood is the original author of this file.
 * Brought to SCA-SW by Piotr Nikiel, June 2018 and significantly cut down
 * to only support what the JTAG algorithm requires.
 * This is the interface which adapts Henk's approach to SCA-SW way of doing things.
 */

#ifndef FLXUPLOAD_H
#define FLXUPLOAD_H

#include <stdint.h>

#include <string>
#include <vector>
#include <list>

#include <Sca/Request.h>
#include <Sca/Sca.h>

namespace HenksJtag
{



typedef int64_t  i64;
typedef uint64_t u64;
typedef uint32_t u32;
typedef uint16_t u16;
typedef uint8_t  u8;




class  FlxUpload
{
 public:
  // C'tor, d'tor
  FlxUpload( Sca::Sca& sca, unsigned int maxGroupSize );
  ~FlxUpload();




  // EC/GBT-SCA
  bool        uploadScaFrame      ( int elink,
                                    int *trid, int chan, int len, int cmd,
                                    u8  *data );
  void        resetScaFrames      ( );
  void        dumpScaFrames       ( );
  bool        addScaFrame         ( int *trid, int chan, int len,
				    int cmd, u8 *data, unsigned int delay=0, bool storeReadBack=false );

  bool        uploadScaFrames     ( int elink=0 );


  /* The following - storing the readback -  is the addition from Piotr.
   * It maybe is slightly illogical to keep the readback in FlxUpload class
   * but this seemed architecturally sound to merge Henk's and SCA-SW interface.
   */
  std::vector<Sca::Reply> getReadBackFrames () const { return m_readBackFrames; }
  void clearReadBack() { m_readBackFrames.clear(); }

 private:

  Sca::Sca& m_sca;
  Sca::SynchronousService& m_synchronousService;
  std::vector<Sca::Request> m_groupedRequests;
  unsigned int m_maxGroupSize;
  std::vector<Sca::Reply> m_readBackFrames;


};

}

#endif // FLXUPLOAD_H
