/* Henk Boterenbrood is the original author of this file.
 * Brought to SCA-SW by Piotr Nikiel, June 2018.
 */

/* -------------------------------------------------------------------------
File   : BitfileReader.h

Descr  : FPGA bit-file (.bit) reader class.

History: 03NOV17; Created.

---------------------------------------------------------------------------- */

#ifndef BITFILEREADER_H
#define BITFILEREADER_H

#include <string>
#include <vector>

namespace HenksJtag
{

typedef unsigned char byte;

// ----------------------------------------------------------------------------

class BitfileReader
{
 public:

  BitfileReader();

  ~BitfileReader();

  byte*        data       ( ) { return _data; }
  unsigned int nbytes     ( ) { return _nbytes; }

  unsigned int fieldCount ( ) { return _field.size(); }
  unsigned int fieldId    ( unsigned int index );
  unsigned int fieldLength( unsigned int index );
  byte*        fieldData  ( unsigned int index );

  void setFile            ( std::string filename, bool skip_data = false );
  void setBlob            ( const std::string& blob );

  std::string fileName    ( ) { return _fileName; }
  bool valid              ( ) { return _valid; }

  void reverseBytes       ( );

 private:
  bool        _valid;
  std::string _fileName;

  typedef struct field
  {
    unsigned int id;
    unsigned int length;
    char        *data;
  } field_t;

  std::vector<field_t> _field;
  byte                *_data;
  unsigned int         _nbytes;
};

// ----------------------------------------------------------------------------

class BitfileException: public std::runtime_error
{
 public:
  BitfileException( std::string str );

  ~BitfileException();

  std::string toString();

 private:
  std::string _str;
  int         _data;
};

}

// ----------------------------------------------------------------------------
#endif // BITFILEREADER_H

