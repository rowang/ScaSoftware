/*
 * ScaSwLogComponents.h
 *
 *  Created on: 12 Dec 2018
 *      Author: pnikiel
 */

#ifndef INCLUDE_SCACOMMON_SCASWLOGCOMPONENTS_H_
#define INCLUDE_SCACOMMON_SCASWLOGCOMPONENTS_H_

#include <LogIt.h>

namespace Sca
{

class LogComponentLevels
{
public:
    static Log::LogComponentHandle adc() { return s_adc; }
    static Log::LogComponentHandle dac() { return s_dac; }
    static Log::LogComponentHandle gpio() { return s_gpio; }
    static Log::LogComponentHandle i2c() { return s_i2c; }
    static Log::LogComponentHandle spi() { return s_spi; }
    static Log::LogComponentHandle jtag() { return s_jtag; }
    static Log::LogComponentHandle sim() { return s_sim; }
    static Log::LogComponentHandle netio() { return s_netio; }
    static Log::LogComponentHandle ss() { return s_ss; }

    static void initializeScaSw(Log::LOG_LEVEL initialLogLevel = Log::LOG_LEVEL::INF);

private:
    static Log::LogComponentHandle s_adc;
    static Log::LogComponentHandle s_dac;
    static Log::LogComponentHandle s_gpio;
    static Log::LogComponentHandle s_i2c;
    static Log::LogComponentHandle s_spi;
    static Log::LogComponentHandle s_jtag;
    static Log::LogComponentHandle s_sim;
    static Log::LogComponentHandle s_netio;
    static Log::LogComponentHandle s_ss;
};

}

#endif /* INCLUDE_SCACOMMON_SCASWLOGCOMPONENTS_H_ */
