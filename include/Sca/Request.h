/*
 * Request.h
 *
 *  Created on: Mar 14, 2017
 *      Author: pnikiel
 */

#ifndef INCLUDE_SCA_REQUEST_H_
#define INCLUDE_SCA_REQUEST_H_

#include <Hdlc/Payload.h>

namespace Sca
{

class Request: public Hdlc::Payload
{
public:
	Request( const Hdlc::Payload& other):
		Hdlc::Payload(other),
		m_trailingPause(0)
	{
	}

	Request( uint8_t channel, uint8_t command, std::initializer_list<uint8_t> data );
	Request( uint8_t channel, uint8_t command, uint8_t* data, uint8_t length = 4 );

	uint8_t transactionId() const { return m_data[0]; }
	void assignTransactionId (uint8_t transactionId);

	uint8_t channelId() const { return m_data[1]; }

	uint8_t command() const { return m_data[3]; }

	// to be removed once we reach stability
	//FIXME
	void setLength(int l) { m_data[2] = l; }

	void setTrailingPause(unsigned int pause) { m_trailingPause = pause; }
	unsigned int trailingPause () const { return m_trailingPause; }
private:
	unsigned int m_trailingPause;
};


}

#endif /* INCLUDE_SCA_REQUEST_H_ */
