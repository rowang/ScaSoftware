/*
 * Defs.h
 *
 *  Created on: May 4, 2016
 *      Author: pnikiel,aikoulou,pmoschov
 */

#ifndef SCA_DEFS_H_
#define SCA_DEFS_H_

namespace Sca
{

  namespace Constants
  {

    enum ChannelIds
    {
      NODE = 0x00,
      SPI = 0x01,
      GPIO = 0x02,
      I2C0 = 0x03,
      I2C1 = 0x04,
      I2C2 = 0x05,
      I2C3 = 0x06,
      I2C4 = 0x07,
      I2C5 = 0x08,
      I2C6 = 0x09,
      I2C7 = 0x0A,
      I2C8 = 0x0B,
      I2C9 = 0x0C,
      I2CA = 0x0D,
      I2CB = 0x0E,
      I2CC = 0x0F,
      I2CD = 0x10,
      I2CE = 0x11,
      I2CF = 0x12,
      JTAG = 0x13,
      ADC = 0x14,
      DAC = 0x15
    };

    enum Commands
    {
      /* NODE - SCA control - commands */
      NC_W_CRA = 0x00,
      NC_R_CRA = 0x01,
      NC_W_CRB = 0x02,
      NC_R_CRB = 0x03,
      NC_W_CRC = 0x04,
      NC_R_CRC = 0x05,
      NC_W_CRD = 0x06,
      NC_R_CRD = 0x07,
	    
      /* ADC commands */
      ADC_GO = 0x02,
      ADC_W_INSEL = 0x50,
      ADC_R_INSEL = 0x51,
      ADC_W_CUREN = 0x60,
      ADC_R_CUREN = 0x61,
	  ADC_W_GAIN = 0x70,
	  ADC_R_GAIN = 0x71,
	  CTRL_R_ID = 0xD1, // note ADC serves the chip ID
	    
      /* GPIO commands */
      GPIO_R_DATAOUT 	= 0x11,
      GPIO_W_DATAOUT	= 0x10,
      GPIO_R_DATAIN	= 0x01,
      GPIO_W_DIRECTION= 0x20,
      GPIO_R_DIRECTION= 0x21,
      GPIO_W_INTENABLE= 0x60,
      GPIO_R_INTENABLE= 0x61,
      GPIO_W_INTSEL	= 0x30,
      GPIO_R_INTSEL	= 0x31,
      GPIO_W_INTTRIG	= 0x40,
      GPIO_R_INTTRIG	= 0x41,
      GPIO_W_INTS	= 0x70,
      GPIO_R_INTS	= 0x71,
      GPIO_W_CLKSEL	= 0x80,
      GPIO_R_CLKSEL	= 0x81,
      GPIO_W_EDGESEL	= 0x90,
      GPIO_R_EDGESEL	= 0x91,

      /* SPI commands */
      SPI_GO = 0x72,
      SPI_W_CTRL = 0x40,
      SPI_R_CTRL = 0x41,
      SPI_W_FREQ = 0x50,
      SPI_R_FREQ = 0x51,
      SPI_W_SS = 0x60,
      SPI_R_SS = 0x61,
      SPI_W_MOSI0 = 0x00,
      SPI_R_MISO0 = 0x01,
      SPI_W_MOSI1 = 0x10,
      SPI_R_MISO1 = 0x11,
      SPI_W_MOSI2 = 0x20,
      SPI_R_MISO2 = 0x21,
      SPI_W_MOSI3 = 0x30,
      SPI_R_MISO3 = 0x31,

      /* I2C commands */
      I2C_W_CTRL = 0x30,
      I2C_R_CTRL = 0x31,
      I2C_R_STR = 0x11,
      I2C_W_MSK = 0x20,
      I2C_R_MSK = 0x21,
      I2C_W_DATA0 = 0x40,
      I2C_R_DATA0 = 0x41,
      I2C_W_DATA1 = 0x50,
      I2C_R_DATA1 = 0x51,
      I2C_W_DATA2 = 0x60,
      I2C_R_DATA2 = 0x61,
      I2C_W_DATA3 = 0x70,
      I2C_R_DATA3 = 0x71,
      I2C_S_7B_W = 0x82,
      I2C_S_7B_R = 0x86,
      I2C_S_10B_W = 0x8A,
      I2C_S_10B_R = 0x8E,
      I2C_M_7B_W = 0xDA,
      I2C_M_7B_R = 0xDE,
      I2C_M_10B_W = 0xE2,
      I2C_M_10B_R = 0xE6,
      //I2C_RMW_AND = , TODO: Uncomment when the new manual is out
      I2C_RMW_OR = 0xC6,
      I2C_RMW_XOR = 0xCA,

      /* DAC commands */
      DAC_W_A = 0x10,
      DAC_R_A = 0x11,
      DAC_W_B = 0x20,
      DAC_R_B = 0x21,
      DAC_W_C = 0x30,
      DAC_R_C = 0x31,
      DAC_W_D = 0x40,
      DAC_R_D = 0x41,

      /* JTAG commands */
      JTAG_W_CTRL = 0x80,
      JTAG_R_CTRL = 0x81,
      JTAG_W_FREQ = 0x90,
      JTAG_R_FREQ = 0x91,
      JTAG_W_TDO0 = 0x00,
      JTAG_R_TDI0 = 0x01,
      JTAG_W_TDO1 = 0x10,
      JTAG_R_TDI1 = 0x11,
      JTAG_W_TDO2 = 0x20,
      JTAG_R_TDI2 = 0x21,
      JTAG_W_TDO3 = 0x30,
      JTAG_R_TDI3 = 0x31,
      JTAG_W_TMS0 = 0x40,
      JTAG_R_TMS0 = 0x41,
      JTAG_W_TMS1 = 0x50,
      JTAG_R_TMS1 = 0x51,
      JTAG_W_TMS2 = 0x60,
      JTAG_R_TMS2 = 0x61,
      JTAG_W_TMS3 = 0x70,
      JTAG_R_TMS3 = 0x71,
      JTAG_ARESET = 0xC0,
      JTAG_GO     = 0xA2,
      JTAG_GO_M   = 0xB0

    };

    enum Errors // reference: GBT-SCA Manual v8, page 14
    {
      GENERIC_ERROR_FLAG = 0x01,
      INVALID_CHANNEL_REQUEST = 0x02,
      INVALID_COMMAND_REQUEST = 0x04,
      INVALID_TRANSACTION_NUMBER_REQUEST = 0x08,
      INVALID_REQUEST_LENGTH = 0x10,
      CHANNEL_DISABLED = 0x20,
      CHANNEL_BUSY = 0x40,
	  COMMAND_IN_TREATMENT = 0x80
    };

    enum Specs
    {
      ADC_CHANNELS_NUM = 32,
      SPI_FREQ_MIN = 156,
      SPI_FREQ_MAX = 20000,
      SPI_TRANSMISSION_BITLENGTH_MAX = 128
    };

    enum AdcChannelIds
    {
      INTERNAL_TEMPERATURE = 31
	};

    enum Spi
    {
      SPI_MODE_0 = 0,
      SPI_MODE_1 = 1,
      SPI_MODE_2 = 2,
      SPI_MODE_3 = 3,
      SPI_MISO_REGISTERS_DISTANCE = 0x10
    };

    enum I2c
    {
      I2C_FREQ_100KHz = 100,
      I2C_FREQ_200KHz = 200,
      I2C_FREQ_400KHz = 400,
      I2C_FREQ_1MHz = 1000,

      I2C_SCLMODE_OPEN_DRAIN = 0x00,
      I2C_SCLMODE_CMOS_OUTPUT = 0x01,

      I2C_NBYTE_1 = 1,
      I2C_NBYTE_2 = 2,
      I2C_NBYTE_3 = 3,
      I2C_NBYTE_4 = 4,
      I2C_NBYTE_5 = 5,
      I2C_NBYTE_6 = 6,
      I2C_NBYTE_7 = 7,
      I2C_NBYTE_8 = 8,
      I2C_NBYTE_9 = 9,
      I2C_NBYTE_10 = 10,
      I2C_NBYTE_11 = 11,
      I2C_NBYTE_12 = 12,
      I2C_NBYTE_13 = 13,
      I2C_NBYTE_14 = 14,
      I2C_NBYTE_15 = 15,
      I2C_NBYTE_16 = 16,

	  I2C_CHANNEL_ID_OFFSET = 3,

      // This is according to I2C standard. The last 3 bits are 2 for the address and
      // the last for Read or Write. 0 for Read and 1 for Write.
      I2C_10B_ADDRESS_OFFSET = 0b01111000
    };

    //Gpio constants
    enum Gpio
    {
      //values taken from the SCA Manual V8.0
      DirectionIn		= 0x00,
      DirectionOut		= 0x01,
      InterruptsEnabled	= 0x01,
      InterruptsDisabled	= 0x00,
      InterruptsOnRisingEdge	= 0x01,
      InterruptsOnFallingEdge	= 0x00,
      SelectInternalClock	= 0x00,
      SelectExternalClock	= 0x01	
    };
    // register names
    enum GpioRegisters
    {
      //these are arbitrary, but only defined here, to allow "global" use
      DataOut		= 0xF8,
      DataIn		= 0xF9,
      Direction	= 0xFA,
      InterruptSelect	= 0xFB,
      InterruptTrigger= 0xFC,
      EdgeSelect	= 0xFD
    };
    // flag names
    enum GpioFlags
    {
      InterruptEnable = 0xFE,
      ClockSelect = 0xFF
    };
    //DAC constants
    enum Dac
    {
      DAC_A,
      DAC_B,
      DAC_C,
      DAC_D
    };
    enum Jtag
    {
      RxEdgeRising  = 0x00,
      RxEdgeFalling = 0x01,
      TxEdgeRising  = 0x00,
      TxEdgeFalling = 0x01,
      MSB_LSB = 0x00,
      LSB_MSB = 0x01,
      TCK_Idle_High = 0x00,
      TCK_Idle_Low = 0x01
    };
  }

}


#endif /* SCA_DEFS_H_ */
