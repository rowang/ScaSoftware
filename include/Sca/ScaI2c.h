/*
 * ScaSpi.h
 *
 *  Created on: Dec 3, 2018
 *      Author: Paris Moschovakos
 */

#pragma once

namespace Sca
{

	class I2c
	{
	public:
		I2c ( SynchronousService& ss ): m_synchronousService( ss ) {}

		uint8_t getControlRegister ( uint8_t i2cMaster );
		void setControlRegister ( uint8_t i2cMaster, uint8_t ctrlReg );
		void setFrequency ( uint8_t i2cMaster, unsigned int frequency );
		unsigned int getFrequency ( uint8_t i2cMaster );
		void setTransmissionByteLength ( uint8_t i2cMaster, uint8_t transmissionByteLength );
		void setSclMode ( uint8_t i2cMaster, bool sclMode );
		bool getSclMode ( uint8_t i2cMaster );

		struct statusRegister
		{
			bool successfulTransaction, sdaLineLevelError, invalidCommandSent, lastOperationNotAcknowledged;
		};

		uint8_t getStatusRegister ( uint8_t i2cMaster );
		statusRegister getStatus( uint8_t i2cMaster, uint8_t statusReg );

		void setDataRegister ( uint8_t i2cMaster, std::vector<uint8_t> &data );
		std::vector<uint8_t> getDataRegister ( uint8_t i2cMaster );

		std::vector<uint8_t> readSingleByte7bit ( uint8_t i2cMaster, uint8_t address );
		uint8_t writeSingleByte7bit ( uint8_t i2cMaster, uint8_t address, uint8_t data );
		std::vector<uint8_t> readMultiByte7bit ( uint8_t i2cMaster, uint8_t address, uint8_t nbytes );
		uint8_t writeMultiByte7bit ( uint8_t i2cMaster, uint8_t address, std::vector<uint8_t> &data );

		std::vector<uint8_t> readSingleByte10bit ( uint8_t i2cMaster, uint16_t address );
		uint8_t writeSingleByte10bit ( uint8_t i2cMaster, uint16_t address, uint8_t data );
		std::vector<uint8_t> readMultiByte10bit ( uint8_t i2cMaster, uint16_t address, uint8_t nbytes );
		uint8_t writeMultiByte10bit ( uint8_t i2cMaster, uint16_t address, std::vector<uint8_t> &data );

		uint8_t write ( uint8_t i2cMaster, bool addressingMode, uint16_t address, std::vector<uint8_t> &data );
		std::vector<uint8_t> read ( uint8_t i2cMaster, bool addressingMode, uint16_t address, uint8_t nbytes );

		// Initial values of an SCA at power up. New values are cached here.
		unsigned int m_frequency = ::Sca::Constants::I2c::I2C_FREQ_100KHz;
		uint8_t m_nbyte = ::Sca::Constants::I2c::I2C_NBYTE_2;
		bool m_sclmode = ::Sca::Constants::I2c::I2C_SCLMODE_OPEN_DRAIN;
		uint8_t m_lasti2cMaster = 0;

	private:
		std::initializer_list<uint8_t>* getPayloads( std::initializer_list<uint8_t> &data );
		SynchronousService& m_synchronousService;
		std::string vectorToHexString( const std::vector<uint8_t>& data );

	};

}
