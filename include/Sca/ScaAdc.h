/*
 * Adc.h
 *
 *  Created on: 29 Mar 2018
 *      Author: pnikiel
 */

#ifndef INCLUDE_SCA_SCAADC_H_
#define INCLUDE_SCA_SCAADC_H_

namespace Sca
{

class Adc
{
public:
  Adc (SynchronousService& ss): m_synchronousService(ss) {}
  ~Adc () {}

  // convert channel is set input line + go.
  // it might speed-up your conversion when the same channel is converted
  // by not sending a setInputLine request
  uint16_t convertChannel (uint8_t channelId);

  uint16_t go ();

  void setInputLine ( unsigned int inputLine );
  unsigned int getInputLine ();

  void setCurrentSourceMask ( uint32_t mask );
  uint32_t getCurrentSourceMask ();

  void setGainCalibration(uint32_t x);
  uint32_t getGainCalibration();

protected:
  SynchronousService& m_synchronousService;
};

}

#endif /* INCLUDE_SCA_SCAADC_H_ */
