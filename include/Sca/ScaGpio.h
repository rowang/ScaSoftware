#ifndef INCLUDE_SCA_SCAGPIO_H_
#define INCLUDE_SCA_SCAGPIO_H_

namespace Sca
{

	class Gpio
	  /*!
	    NOTE: our GPIO API doesn't use event-mode notifications.
	   */
	{
	private:
		//LOW-LEVEL
		uint32_t getReadCommandFromRegister(uint32_t gpioRegister);
		uint32_t getWriteCommandFromRegister(uint32_t gpioRegister);
		void sendWriteDataToRegister(uint32_t data,uint32_t gpioRegister);
		uint32_t sendReadRegister(uint32_t gpioRegister);

	public:
		Gpio (SynchronousService& ss): m_synchronousService(ss) {}

		//USER LEVEL (using low-level functions)
		//for registers
		uint32_t getRegister(uint32_t gpioRegister);
		void setRegister(uint32_t gpioRegister,uint32_t data);

		bool getRegisterBitValue(uint32_t gpioRegister,int pinIndex);
		void setRegisterBitToValue(uint32_t gpioRegister, int pinIndex, bool setValue);

		std::vector<bool> getRegisterRangeValue(uint32_t gpioRegister,int fromPin,int toPin);
		void setRegisterRangeToValue(uint32_t gpioRegister, int fromPin,int toPin,bool setValue);


	private:
		SynchronousService& m_synchronousService;
		boost::mutex m_single_access_mutex_GPIO;
	};

}

#endif /* INCLUDE_SCA_SCAGPIO_H_ */
