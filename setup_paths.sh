# This script primarily serves to configure your SCA-SW dependencies for usage with
# TDAQ software ( netio, felixbase, etc)
# Don't forget to source setup.sh from your felix installation directory

# You should source this file, not execute it!

# Authors: Paris and Piotr

PATH_TO_LCG="/cvmfs/sft.cern.ch/lcg/releases/"

usage()
{
	echo
	echo "Usage"
	echo
	echo "[-h]: help"
	echo "[-8]: Use gcc 8.3.0 as the selected compiler (Default: gcc 6.2.0)" 
	echo
}

epilogue()
{
	export PATH=/cvmfs/sft.cern.ch/lcg/contrib/CMake/3.11.1/Linux-x86_64/bin:$PATH
	echo "Dependencies configured. Please also source the setup.sh from your FELIX installation directory."
}
	
grep -e "CentOS" /etc/redhat-release
rc=$?
if [ $rc == 0 ]
then
	echo "We're on CentOS..."
	if [ "$1" == "-h" ]
	then
		usage
	elif [ "$1" == "-8" ]
	then
		echo "Using gcc83..."
		export CXX="${PATH_TO_LCG}gcc/8.3.0/x86_64-centos7/bin/c++"
		export CC="${PATH_TO_LCG}gcc/8.3.0/x86_64-centos7/bin/gcc"
		export BOOST_HEADERS="${PATH_TO_LCG}LCG_95/Boost/1.69.0/x86_64-centos7-gcc8-opt/include"
		export BOOST_LIB_DIRECTORIES="${PATH_TO_LCG}LCG_95/Boost/1.69.0/x86_64-centos7-gcc8-opt/lib"  
		export BOOST_LIBS="-lboost_regex -lboost_chrono -lboost_program_options -lboost_thread -lboost_system -lboost_filesystem"
		epilogue
	else
		echo "Using gcc62..."
		export CXX="${PATH_TO_LCG}gcc/6.2.0/x86_64-centos7/bin/c++"
		export CC="${PATH_TO_LCG}gcc/6.2.0/x86_64-centos7/bin/gcc"
		export BOOST_HEADERS="${PATH_TO_LCG}LCG_87/Boost/1.62.0/x86_64-centos7-gcc62-opt/include/boost-1_62"
		export BOOST_LIB_DIRECTORIES="${PATH_TO_LCG}LCG_87/Boost/1.62.0/x86_64-centos7-gcc62-opt/lib"
		export BOOST_LIBS="-lboost_regex-gcc62-mt-1_62 -lboost_chrono-gcc62-mt-1_62 -lboost_program_options-gcc62-mt-1_62 -lboost_thread-gcc62-mt-1_62 -lboost_system-gcc62-mt-1_62 -lboost_filesystem-gcc62-mt-1_62"
		epilogue
	fi
fi
