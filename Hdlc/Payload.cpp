#include <boost/lexical_cast.hpp>

#include <Hdlc/Payload.h>

#include <sstream>
#include <algorithm>
#include <ScaCommon/except.h>
#include <iostream>

namespace Hdlc
{

Payload::Payload( const std::vector<uint8_t>& data ):
    m_dataSize( data.size () )
{
	if (m_dataSize > 8)
		THROW_WITH_ORIGIN(std::runtime_error,"payload too big size="+boost::lexical_cast<std::string>(data.size()));
	if (m_dataSize%2 != 0)
		THROW_WITH_ORIGIN(std::runtime_error,"payload size not multiple of 2");
    std::copy( data.begin(), data.end(), m_data );
}

 Payload::Payload(
 		std::initializer_list<uint8_t> data1,
 		std::initializer_list<uint8_t> data2):
 				m_dataSize (data1.size() + data2.size())
 {
 	if (data1.size() != 4)
 		scasw_throw_runtime_error_with_origin("First part of data shall be of size 4");
 	if (data2.size() > 4)
 		scasw_throw_runtime_error_with_origin("Second part of data shall be no bigger than 4 octets");
    if (m_dataSize%2 != 0)
        scasw_throw_runtime_error_with_origin("Payload size not multiple of 2");

 	std::copy( data1.begin(), data1.end(), m_data );
 	std::copy( data2.begin(), data2.end(), m_data+4 );
 }

 Payload::Payload (const uint8_t* data, size_t length):
				 m_dataSize(length)
 {
	 if (m_dataSize > 8)
		 THROW_WITH_ORIGIN(std::runtime_error,"payload too big size="+boost::lexical_cast<std::string>(length));
	 if (m_dataSize%2 != 0)
		 THROW_WITH_ORIGIN(std::runtime_error,"payload size not multiple of 2");
	 std::copy( data, data+length, m_data );
 }

 Payload::Payload (std::initializer_list<uint8_t> data1, uint8_t* data2, uint8_t length):
				 m_dataSize(length+4)
 {
	 if (data1.size() != 4)
		scasw_throw_runtime_error_with_origin("First part of data shall be of size 4");
	 if (m_dataSize > 8)
		 THROW_WITH_ORIGIN(std::runtime_error,"payload too big");
	 if (m_dataSize%2 != 0)
		 THROW_WITH_ORIGIN(std::runtime_error,"payload size not multiple of 2");

	 std::copy( data1.begin(), data1.end(), m_data );
	 std::copy( data2, data2+length, m_data+4 );
 }

std::string Payload::toString() const
{
	std::ostringstream out;
	out << "[tr_id=" << (unsigned int)m_data[0] << std::hex << " ch=" << (unsigned int)m_data[1];
	out << " b2=" << (unsigned int)m_data[2] << " b3=" << (unsigned int)m_data[3];
	out << " data=<";
	for (unsigned int i=4; i<m_dataSize; i++)
		out << (unsigned int)m_data[i] << " ";
	out << ">]";
	return out.str();
}

}
