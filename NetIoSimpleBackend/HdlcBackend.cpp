#include <LogIt.h>
#include <algorithm>
#include <boost/bind.hpp>

#include <NetIoSimpleBackend/HdlcBackend.h>
#include <NetIoSimpleBackend/SimpleNetioSpecificAddress.h>

#include <iostream>

#include <felixbase/client.hpp>
#include <ScaCommon/ScaSwLogComponents.h>

// http://atlas-felix.cern.ch/netio/

using Sca::LogComponentLevels;

namespace NetIoSimpleBackend
{

const unsigned int NETIO_FRAME_HDLC_START_OFFSET = 8;  // HDLC frame shall be placed starting from this index
const unsigned int HDLC_FRAME_PAYLOAD_OFFSET = 2;   // HDLC payload offset in HDLC frame
const unsigned int HDLC_FRAME_TRAILER_SIZE = 2;   // HDLC trailer size in HDLC frame

/* The following constant relates to Henk B. method of achieving (relatively small - up to say 50 us) delays on the HDLC-encoded elink.
 * A good reading on Henk's method is in: https://its.cern.ch/jira/browse/FLX-581
 * The constant below defines how many zeros to put per microsecond of delay.
 * HDLC Elink throughput is 80Mbps, so 80 bits goes through each 1us.  80bits is 10 bytes that's why we insert 10 bytes to delay by 1 us.
 */
const unsigned int FLX_ZEROS_PER_MICROSECOND = 10;

HdlcBackend::HdlcBackend (const std::string& specificAddress):
        		m_originalSpecificAddress(specificAddress),
				m_specificAddress( specificAddress ),
				m_netioContext("posix"),
				m_netioRequestSocket( &m_netioContext ),
				m_netioReplySocket( &m_netioContext, boost::bind(&HdlcBackend::replyHandler, this, _1, _2) ),
				m_seqnr (7), // will be incremented before 1st use, and it is modulo 8,
                m_requestSocketConnected(false),
                m_replySocketSubscribed(false)
{

	LOG(Log::INF, LogComponentLevels::netio()) << "Opening backend for hostname=" << m_specificAddress.felixHostname()
			<< ", port to SCA: " << m_specificAddress.portToSca()
			<< ", SCA elink: TX: 0x" << std::hex << m_specificAddress.elinkIdTx() << " RX: 0x" << std::hex << m_specificAddress.elinkIdRx();
	m_netioThread = boost::thread( [this](){ netioMainThread(); } );

	try
	{
	    m_netioRequestSocket.connect(netio::endpoint( m_specificAddress.felixHostname(), m_specificAddress.portToSca()));
	    m_requestSocketConnected = true;
	    LOG(Log::INF, LogComponentLevels::netio()) << "Subscribing to elink: " << std::hex << (int)m_specificAddress.elinkIdRx();
	    m_netioReplySocket.subscribe( m_specificAddress.elinkIdRx(), netio::endpoint( m_specificAddress.felixHostname(), m_specificAddress.portFromSca() ));
	    m_replySocketSubscribed = true;
	    usleep(100000);
	    sendHdlcControl( Hdlc::HdlcControlCodes::RESET );
	}
	catch (const std::exception &e )
	{
	    LOG(Log::ERR, LogComponentLevels::netio()) << "Exception: " << e.what();
	    cleanUp();
	    throw e;
	}
}

HdlcBackend::~HdlcBackend ()
{
    cleanUp ();
}

std::string vectorAsHex (const std::vector<uint8_t> & v )
{
    std::stringstream ss;
    for (uint8_t octet : v)
    {
        ss.width(2);
        ss.fill('0');
        ss << std::hex << (unsigned int)octet << ' ';
    }
    return ss.str();
}

void HdlcBackend::send (const Hdlc::Payload &request )
{
    std::vector<Hdlc::Payload> requests ({ request });
    std::vector<unsigned int> times ({0});
    this->send( requests, times );
}

void HdlcBackend::send (
          const std::vector<Hdlc::Payload> &requests,
          const std::vector<unsigned int> times
      )
{

    if (requests.size() != times.size())
        scasw_throw_runtime_error_with_origin("requests vector has different size than times vector!");

    std::vector<unsigned char> netioFrame;
    netioFrame.reserve( 8192 );

    // comment: send() synchronization is very important to make sure that HDLC
    // sequence numbers - stored as m_seqnr - are guaranteed sequential.
    std::lock_guard<decltype(m_transmitSynchronizationMutex)> lock (m_transmitSynchronizationMutex);

    for (unsigned int i=0; i<requests.size(); ++i)
    {
        const Hdlc::Payload& request = requests[i];
        m_seqnr = (m_seqnr+1)%8;
        std::vector<uint8_t> hdlc_encoded_frame( HDLC_FRAME_PAYLOAD_OFFSET + request.size() + HDLC_FRAME_TRAILER_SIZE, 0 );
        felix::hdlc::encode_hdlc_msg_header( &hdlc_encoded_frame[0], /*HDLC address, always 0 for SCA*/0, m_seqnr);
        std::copy( request.cbegin(), request.cend(), &hdlc_encoded_frame[ HDLC_FRAME_PAYLOAD_OFFSET] );
        felix::hdlc::encode_hdlc_trailer(
                &hdlc_encoded_frame[ HDLC_FRAME_PAYLOAD_OFFSET + request.size()], /* where to put the trailer */
                &hdlc_encoded_frame[0], /* pointer to the beginning of HDLC frame*/
                request.size() + HDLC_FRAME_PAYLOAD_OFFSET);


        felix::base::ToFELIXHeader header;
        header.length = hdlc_encoded_frame.size();
        header.reserved = 0;
        header.elinkid = m_specificAddress.elinkIdTx();
        uint8_t * const headerPtr = reinterpret_cast<uint8_t*>(&header);

        std::vector<uint8_t> thisChunk ( hdlc_encoded_frame.size() + sizeof(header), 0 );
        std::copy(
                headerPtr,
                headerPtr + sizeof(header),
                &thisChunk[0] );

        std::copy(
                hdlc_encoded_frame.begin(),
                hdlc_encoded_frame.end(),
                &thisChunk[sizeof header] );

        netioFrame.insert( netioFrame.end(), thisChunk.begin(), thisChunk.end() );

        if (times[i] > 0)
        {
            /* We will glue a block of zeros after the contents for a delay in Henk B. method */
            const unsigned int numBytes = FLX_ZEROS_PER_MICROSECOND * times[i];
            header.length = numBytes;
            netioFrame.insert( netioFrame.end(), headerPtr, headerPtr+sizeof header );
            netioFrame.insert( netioFrame.end(), numBytes, 0);
        }

        m_statistician.onFrameSent();
    }

    LOG(Log::TRC, LogComponentLevels::netio()) << "sending netio request frame, group_size=" << requests.size() << ", frame_size=" << netioFrame.size();

    LOG(Log::TRC, LogComponentLevels::netio()) << "contents: " << vectorAsHex(netioFrame);


    netio::message msg( &netioFrame[0], netioFrame.size() );

    m_netioRequestSocket.send( msg );

}

void HdlcBackend::netioMainThread(void)
{
	LOG(Log::INF, LogComponentLevels::netio()) << "Inside netio thread for address: " << m_originalSpecificAddress;
    try
    {
        m_netioContext.event_loop()->run_forever();
    }
    catch (const std::exception& e)
    {
        LOG(Log::ERR, LogComponentLevels::netio()) << "From netio run_forever(): " << e.what();
    }
}

void HdlcBackend::replyHandler( netio::endpoint&, netio::message& m )
{
	LOG(Log::TRC, LogComponentLevels::netio()) << "received reply";
	std::vector<uint8_t> netioFrame ( m.data_copy() );

	felix::base::FromFELIXHeader *header = reinterpret_cast<felix::base::FromFELIXHeader *> (&netioFrame[0]);

	if (header->length < 16) // too small to be SCA frame so assume this is some HDLC there ...
	{
	    auto it = netioFrame.begin() + sizeof(felix::base::FromFELIXHeader);
	    std::stringstream ss;
	    while (it != netioFrame.end())
	    {
	        ss << std::hex << (unsigned int)*it << " ";
	        it++;
	    }
	    LOG(Log::WRN, LogComponentLevels::netio()) << "Received tiny frame, too small to be SCA but it might be HDLC control, HDLC bytes are: " << ss.str();
	    return;
	}

	if (netioFrame.size() < 16 || netioFrame.size() > 24)  // the smallest SCA reply (4 bytes) is 16 bytes of netio frame minimum - 8 for netio header and another 4 for HDLC header and trailer
	{
		LOG(Log::ERR, LogComponentLevels::netio()) << "ignoring possibly broken frame out of permitted size. (len" << netioFrame.size() << ")";
		return;
	}

	// How to check the HDLC checksum?
	std::vector<uint8_t> correctTrailer (HDLC_FRAME_TRAILER_SIZE, 0);
	felix::hdlc::encode_hdlc_trailer(
	        &correctTrailer[0],
	        &netioFrame[NETIO_FRAME_HDLC_START_OFFSET],
	        netioFrame.size() - NETIO_FRAME_HDLC_START_OFFSET - HDLC_FRAME_TRAILER_SIZE);

	auto wrongChecksumIndicator = std::mismatch(
	        std::begin(correctTrailer),
	        std::end(correctTrailer),
	        std::begin(netioFrame) + netioFrame.size() - HDLC_FRAME_TRAILER_SIZE);
	if (wrongChecksumIndicator.first != std::end(correctTrailer))
	{
	    LOG(Log::ERR, LogComponentLevels::netio()) << "HDLC checksum mismatch on incoming frame, discarding";
	    return;
	}

	// Cut the Address and the Control field from the HDLC frame | and the FCS at the end -- Paris
	uint8_t* hdlcPayloadPtr = &netioFrame[ NETIO_FRAME_HDLC_START_OFFSET + HDLC_FRAME_PAYLOAD_OFFSET ];

	try // the following may throw if the reply is mutilated, too big, etc.
	{
        Hdlc::Payload payload( hdlcPayloadPtr, netioFrame.size() - NETIO_FRAME_HDLC_START_OFFSET - HDLC_FRAME_PAYLOAD_OFFSET - HDLC_FRAME_TRAILER_SIZE);
        std::for_each( m_receivers.begin(), m_receivers.end(), [&payload]( ReceiveCallBack& cb){ cb(payload); });
	}
	catch (const std::exception& e)
	{
	    LOG(Log::TRC, LogComponentLevels::netio()) << "Ignoring reply: " << e.what();
	}
	m_statistician.onFrameReceived();
}

void HdlcBackend::sendHdlcControl (uint8_t hdlcControl)
{
    // Both CONNECT and RESET HDLC OPs reset the sequence of HDLC   
    m_seqnr = 7;


    felix::base::ToFELIXHeader header;

    std::vector<unsigned char> netioFrame ( sizeof(header) + HDLC_FRAME_PAYLOAD_OFFSET + HDLC_FRAME_TRAILER_SIZE );

    header.length = netioFrame.size() - sizeof(header);
    header.reserved = 0;
    header.elinkid = m_specificAddress.elinkIdTx();

    std::copy(
            reinterpret_cast<uint8_t*>(&header),
            reinterpret_cast<uint8_t*>(&header)+sizeof header,
            netioFrame.begin() );
	felix::hdlc::encode_hdlc_ctrl_header( &netioFrame[ sizeof header ], 0, hdlcControl );
	felix::hdlc::encode_hdlc_trailer(
	        &netioFrame[0] + sizeof(header) + HDLC_FRAME_PAYLOAD_OFFSET, /* where to put the trailer */
	        &netioFrame[0] + sizeof(header),
	        HDLC_FRAME_PAYLOAD_OFFSET);
	netio::message message( &netioFrame[0], netioFrame.size() );
	LOG(Log::INF, LogComponentLevels::netio()) << "Sending control: " << std::hex << (unsigned int)hdlcControl;
	LOG(Log::TRC, LogComponentLevels::netio()) << "Size: " << netioFrame.size() << " contents: " << vectorAsHex(netioFrame);
	m_netioRequestSocket.send(message);
}

void HdlcBackend::cleanUp ()
{
    LOG(Log::INF, LogComponentLevels::netio()) << "Disconnecting netio for address=" << m_originalSpecificAddress;

    if (m_replySocketSubscribed)
    {
        m_netioReplySocket.unsubscribe( m_specificAddress.elinkIdRx(), netio::endpoint( m_specificAddress.felixHostname(), m_specificAddress.portFromSca() ));
        m_replySocketSubscribed = false;
    }
    if (m_requestSocketConnected)
    {
        m_netioRequestSocket.disconnect();
        m_requestSocketConnected = false;
    }
    m_netioContext.event_loop()->stop();
    LOG(Log::INF, LogComponentLevels::netio()) << "Waiting to join the netio thread";
    m_netioThread.join();
    LOG(Log::TRC, LogComponentLevels::netio()) << "cleanUp() finished";
}

}
