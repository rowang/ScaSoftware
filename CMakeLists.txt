project(ScaSoftware)
cmake_minimum_required(VERSION 2.8)

option(STANDALONE_SCA_SOFTWARE "Whether to build demonstrator executable" OFF)
option(HAVE_NETIO              "Whether to build backends which require netio" OFF)
option(STANDALONE_WITH_EVENT_RECORDER "Whether to include Event Recording in stand-alone builds" OFF)
option(PULL_EVENT_RECORDER     "Whether to download the Event Recorder library in any case" OFF)

add_definitions(-Wall)

include_directories( $ENV{BOOST_HEADERS} )

if (HAVE_NETIO)
   include_directories( $ENV{FELIX_ROOT}/include )
   link_directories( $ENV{FELIX_ROOT}/lib )
   add_definitions( -DHAVE_NETIO )
endif()

if(PULL_EVENT_RECORDER)
		if(NOT EXISTS ${PROJECT_SOURCE_DIR}/VeryHipEvents )
			execute_process(COMMAND git clone https://github.com/pnikiel/VeryHipEvents.git ${PROJECT_SOURCE_DIR}/VeryHipEvents )
		else( NOT EXISTS ${PROJECT_SOURCE_DIR}/VeryHipEvents )
			message(AUTHOR_WARNING "A directory for VeryHipEvents already exists. Skipping cloning.")
		endif( NOT EXISTS ${PROJECT_SOURCE_DIR}/VeryHipEvents )
        include_directories( ${PROJECT_SOURCE_DIR}/VeryHipEvents/include )
        add_subdirectory( VeryHipEvents )
        add_definitions( -DWITH_EVENT_RECORDER )
	set( VERY_HIP_EVENTS_SRCS $<TARGET_OBJECTS:VeryHipEvents> )	
endif(PULL_EVENT_RECORDER)

if( STANDALONE_SCA_SOFTWARE )
  include_directories(include)
  include_directories(include/LogIt)  
  
  set (LOGIT_HAS_BOOSTLOG      FALSE)
  set (LOGIT_HAS_UATRACE       FALSE)
  set (LOGIT_HAS_STDOUTLOG     TRUE)
  add_subdirectory(LogIt)
  
if( STANDALONE_WITH_EVENT_RECORDER )
	if(NOT EXISTS ${PROJECT_SOURCE_DIR}/VeryHipEvents )
		execute_process(COMMAND git clone https://github.com/pnikiel/VeryHipEvents.git ${PROJECT_SOURCE_DIR}/VeryHipEvents )
	else( NOT EXISTS ${PROJECT_SOURCE_DIR}/VeryHipEvents )
		message(AUTHOR_WARNING "A directory for VeryHipEvents already exists. Skipping cloning.")
	endif( NOT EXISTS ${PROJECT_SOURCE_DIR}/VeryHipEvents )
	include_directories( ${PROJECT_SOURCE_DIR}/VeryHipEvents/include )
	add_subdirectory( VeryHipEvents )
	add_definitions( -DWITH_EVENT_RECORDER )
endif( STANDALONE_WITH_EVENT_RECORDER )
  
  add_definitions( -std=c++0x )
  add_subdirectory( Demonstrators )

endif()


if (HAVE_NETIO)
  file(GLOB NETIO_SIMPLE_BACKEND_SRCS
  NetIoSimpleBackend/*.cpp
  )
endif()

file(GLOB SRCS
  Hdlc/*.cpp
  Sca/*.cpp
  ScaSimulator/*.cpp
  HenksJtag/*.cpp
  ScaCommon/*.cpp
  )


add_library( ScaSoftware OBJECT ${SRCS} ${NETIO_SIMPLE_BACKEND_SRCS}   )

