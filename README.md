
[![build status](https://gitlab.cern.ch/atlas-dcs-common-software/ScaSoftware/badges/master/build.svg)](https://gitlab.cern.ch/atlas-dcs-common-software/ScaSoftware/commits/master)

Quick startup guide
-------------------

For standalone SCA SW (which will let you run some demonstrators to talk to SCA's peripherals), just use

build_standalone.sh

Note that:
1. By default, the software builds with netio and other parts of the TDAQ FELIX Software. 
You can remove netio dependency by changing, in build_standalone, HAVE_NETIO to 0.

2. The easiest way to get netio and TDAQ FELIX is to get the distro from:
https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/dist/software/apps/4.x/

The current supported package is for CC7 in the link below:
 https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/dist/software/apps/4.x/felix-04-00-03-x86_64-centos7-gcc49-opt.tar.gz

3. Please note that you should use exactly same compiler as the one used for TDAQ FELIX chain. For that source the setup_paths.sh to define your environment.
Don't forget to also source the setup.sh from your FELIX installation directory.


