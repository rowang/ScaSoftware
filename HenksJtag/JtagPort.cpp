/* Henk Boterenbrood is the original author of this file.
 * Brought to SCA-SW by Piotr Nikiel, June 2018.
 */

#include <iostream>
#include <iomanip>
#include <unistd.h>

#include <HenksJtag/JtagPort.h>
#include <HenksJtag/flxdefs.h>

namespace HenksJtag
{

const int JTAG_WR[] = { SCA_JTAG_WR_TDO0, SCA_JTAG_WR_TDO1,
                        SCA_JTAG_WR_TDO2, SCA_JTAG_WR_TDO3 };
const int JTAG_RD[] = { SCA_JTAG_RD_TDI0, SCA_JTAG_RD_TDI1,
                        SCA_JTAG_RD_TDI2, SCA_JTAG_RD_TDI3 };

// ----------------------------------------------------------------------------

JtagPort::JtagPort( FlxUpload &fup, int elinknr )
  : _state( TEST_LOGIC_RESET ),
    _irLen( 0 ),
    _progress( false ),
    _elinknr( elinknr ),
    _trid( 1 ),
    _ctrlReg( 0x0000 ),
    _goDelay( 20 ), // Matches 1MHz JTAG clock
    _fup( fup )
{
  // NB: Control Register bit 11 'MSB/LSB' (0x0800) here initialised to 0;
  //     GBT-SCA docu (v8.2-draft) says "bits transmitted msb to lsb" if 0,
  //     but the opposite is true (Henk B, 23 Nov 2017)
}

// ----------------------------------------------------------------------------

JtagPort::~JtagPort()
{
}

// ----------------------------------------------------------------------------

void JtagPort::enable( int freq_mhz )
{
  u8 data[4];

  // Piotr: no need to enable JTAG channel - this is done in Sca::Sca ctr.

  // Configure GBT-SCA JTAG channel (standard; LEN=128 (represented by value 0)
  _ctrlReg &= ~0x007F;
  mapInt2Bytes( _ctrlReg, data );
  _fup.uploadScaFrame( _elinknr, &_trid, SCA_DEV_JTAG, 4,
                       SCA_JTAG_WR_CTRL, data );
  // Frequency of 20MHz: f=2*10000000/(DIV+1) --> DIV=0
  // (NB: LSByte is in data[2] and MSByte in data[3])
  u8 div = 19; // Default 1MHz
  if( freq_mhz == 20 )
    div = 0;
  else if( freq_mhz == 10 )
    div = 1;
  else if( freq_mhz == 5 )
    div = 3;
  else if( freq_mhz == 4 )
    div = 4;
  else if( freq_mhz == 2 )
    div = 9;
  else if( freq_mhz == 1 )
    div = 19;
  else
    scasw_throw_runtime_error_with_origin("Invalid bitrate. Must be from the set {20,10,5,4,2,1} MHz}");
  _goDelay = 20/freq_mhz;
  memset( data, 0, 4 );
  data[2] = div;
  _fup.uploadScaFrame( _elinknr, &_trid, SCA_DEV_JTAG, 4,
                       SCA_JTAG_WR_FREQ, data );
  usleep( 1000 );

  _fup.resetScaFrames();

  // Reset GBT-SCA JTAG TDO and TMS registers
  addResetTdoTms();

  _fup.uploadScaFrames( _elinknr );
}

// ----------------------------------------------------------------------------

void JtagPort::gotoState( state_t s )
{
  uint32_t tms;
  int      nbits = setState( s, &tms );
  //std::cout << "state " << s << ": "
  //          << nbits << " bits, tms=" << tms << std::endl;
  if( nbits <= 0 ) return;

  _fup.resetScaFrames();

  // Configure the number of bits
  u8 data[4];
  _ctrlReg &= ~0x007F;
  _ctrlReg |= (nbits & 0x7F);
  mapInt2Bytes( _ctrlReg, data );
  _fup.addScaFrame( &_trid, SCA_DEV_JTAG, 4, SCA_JTAG_WR_CTRL, data );

  // Configure TDO and TMS
  memset( data, 0, 4 );
  _fup.addScaFrame( &_trid, SCA_DEV_JTAG, 4, SCA_JTAG_WR_TDO0, data );
  mapInt2Bytes( tms, data );
  _fup.addScaFrame( &_trid, SCA_DEV_JTAG, 4, SCA_JTAG_WR_TMS0, data );

  // Shift the bits
  memset( data, 0, 4 );
  _fup.addScaFrame( &_trid, SCA_DEV_JTAG, 2, SCA_JTAG_GO, data, (_goDelay*8*10)/4  );

  _fup.uploadScaFrames( _elinknr );
}

// ----------------------------------------------------------------------------

void JtagPort::shiftIr( int instr )
{
  gotoState( SHIFT_IR );

  _fup.resetScaFrames();

  // Configure number of bits: the single device's instruction register length
  u8 data[4];
  _ctrlReg &= ~0x007F;
  _ctrlReg |= (_irLen & 0x7F);
  mapInt2Bytes( _ctrlReg, data );
  _fup.addScaFrame( &_trid, SCA_DEV_JTAG, 4, SCA_JTAG_WR_CTRL, data );

  // INFO FOR POTENTIAL EXTENSION TO MULTIPLE DEVICES:
  // In case of multiple devices ('numDevices') in chain put 'instr'
  // in selected device 'deviceIndex' and select BYPASS for all other devices;
  // also take that into account in shiftDr():
  // a single bit to add per device in BYPASS.
  //int pre=0; // Number of 1-bits to shift *before* 'instr'
  //for(int dev=deviceIndex+1; dev<numDevices; dev++)
  //  pre+=devices[dev].irlen;  // Calculate number of pre BYPASS bits
  //int post=0; // Number of 1-bits to shift *after* 'instr'
  //for(int dev=0; dev<deviceIndex; dev++)
  //  post+=devices[dev].irlen; // Calculate number of post BYPASS bits

  // Configure TDO
  mapInt2Bytes( instr, data );
  _fup.addScaFrame( &_trid, SCA_DEV_JTAG, 4, SCA_JTAG_WR_TDO0, data );

  // Configure TMS: exit SHIFT_IR state on the final bit
  uint32_t tms = (1 << (_irLen-1));
  mapInt2Bytes( tms, data );
  _fup.addScaFrame( &_trid, SCA_DEV_JTAG, 4, SCA_JTAG_WR_TMS0, data );

  // Shift the bits
  memset( data, 0, 4 );
  _fup.addScaFrame( &_trid, SCA_DEV_JTAG, 2, SCA_JTAG_GO, data, _goDelay*8*10 );

  // DEBUG
  //_fup.addScaFrame( &_trid, SCA_DEV_JTAG, 0, SCA_JTAG_RD_TMS0, data );
  //_fup.addScaDelay( 1*10 ); // Required!

  _fup.uploadScaFrames( _elinknr );

  // We left the SHIFT_IR state
  //nextState( 1 );
  _state = EXIT1_IR;
  gotoState( RUN_TEST_IDLE );
}

// ----------------------------------------------------------------------------

void JtagPort::shiftDr( int nbits, u8 *tdo, bool get_tdi )
{
  gotoState( SHIFT_DR );

  _fup.resetScaFrames();

  // Reset GBT-SCA JTAG TDO and TMS registers
  addResetTdoTms();

  // Configure the number of bits: 128
  _ctrlReg &= ~0x007F;
  u8 data[4];
  mapInt2Bytes( _ctrlReg, data );
  _fup.addScaFrame( &_trid, SCA_DEV_JTAG, 4, SCA_JTAG_WR_CTRL, data );

  _fup.uploadScaFrames( _elinknr );

  int bytes_todo = nbits/8;
  int bytes_done = 0;
  int percentage = 0;
  if( _progress )
    std::cout << "Uploading...     ";

  int index = 0;
  while( nbits > 128 )
    {
      _fup.resetScaFrames();

      // Accumulate up to 'max_cnt' groups of commands into a single DMA
      int max_cnt = 50, cnt = 0;
      while( cnt < max_cnt && nbits > 128 )
        {
          if( tdo )
            {
              for( int i=0; i<4; ++i )
                {
                  data[2] = tdo[index++];
                  data[3] = tdo[index++];
                  data[0] = tdo[index++];
                  data[1] = tdo[index++];
                  _fup.addScaFrame( &_trid, SCA_DEV_JTAG, 4,
                                    JTAG_WR[i], data );
                }
            }

          // Shift the bits
          memset( data, 0, 4 );
          _fup.addScaFrame( &_trid, SCA_DEV_JTAG, 2, SCA_JTAG_GO, data, _goDelay*8*10 );

          if( get_tdi )
            {
              for( int i=0; i<4; ++i )
                {
                  int id = 254; // Transaction-ID to filter on
                  _fup.addScaFrame( &id, SCA_DEV_JTAG, 0, JTAG_RD[i], data, 1*10, get_tdi);
                }
            }

          nbits -= 128;
          bytes_done += 16;
          ++cnt;
        }

      _fup.uploadScaFrames( _elinknr );

      if( _progress )
        // Display percentage done
        displayPercentage( &percentage, bytes_done, bytes_todo );
    }

  _fup.resetScaFrames();

  // Final bits (128 bits or less)
  _ctrlReg &= ~0x007F;
  if( nbits < 128 )
    _ctrlReg |= (nbits & 0x7F);
  mapInt2Bytes( _ctrlReg, data );
  _fup.addScaFrame( &_trid, SCA_DEV_JTAG, 4, SCA_JTAG_WR_CTRL, data );

  if( tdo )
    {
      for( int i=0; i<4; ++i )
        if( nbits > i*32 )
          {
            data[2] = tdo[index++];
            data[3] = tdo[index++];
            data[0] = tdo[index++];
            data[1] = tdo[index++];
            _fup.addScaFrame( &_trid, SCA_DEV_JTAG, 4, JTAG_WR[i], data );
          }
    }

  // Configure TMS: exit SHIFT_DR state on the final bit
  uint32_t tms = (1 << ((nbits-1) & 0x1F));
  mapInt2Bytes( tms, data );
  if( nbits <= 32 )
    _fup.addScaFrame( &_trid, SCA_DEV_JTAG, 4, SCA_JTAG_WR_TMS0, data );
  else if( nbits <= 64 )
    _fup.addScaFrame( &_trid, SCA_DEV_JTAG, 4, SCA_JTAG_WR_TMS1, data );
  else if( nbits <= 96 )
    _fup.addScaFrame( &_trid, SCA_DEV_JTAG, 4, SCA_JTAG_WR_TMS2, data );
  else
    _fup.addScaFrame( &_trid, SCA_DEV_JTAG, 4, SCA_JTAG_WR_TMS3, data );

  // Shift the bits
  memset( data, 0, 4 );
  _fup.addScaFrame( &_trid, SCA_DEV_JTAG, 2, SCA_JTAG_GO, data, _goDelay*8*10 );

  if( get_tdi )
    {
      for( int i=0; i<4; ++i )
        if( nbits > i*32 )
          {
            int id = 254; // Transaction-ID to filter on
            _fup.addScaFrame( &id, SCA_DEV_JTAG, 0, JTAG_RD[i], data, 1*10, get_tdi );
          }
    }

  _fup.uploadScaFrames( _elinknr );

  bytes_done += (nbits+7)/8;
  if( _progress )
    // Display percentage done
    displayPercentage( &percentage, bytes_done, bytes_todo );

  // We left the SHIFT_DR state
  //nextState( 1 );
  _state = EXIT1_DR;
  gotoState( RUN_TEST_IDLE );
}

// ----------------------------------------------------------------------------

void JtagPort::shift( int nbits, bool get_tdi )
{
  // Generate 'nbits' clock cycles in the state we're in
  _fup.resetScaFrames();

  // Reset GBT-SCA JTAG TDO and TMS registers
  addResetTdoTms();

  // Configure the number of bits: 128
  _ctrlReg &= ~0x007F;
  u8 data[4];
  mapInt2Bytes( _ctrlReg, data );
  _fup.addScaFrame( &_trid, SCA_DEV_JTAG, 4, SCA_JTAG_WR_CTRL, data );

  _fup.uploadScaFrames( _elinknr );

  while( nbits > 128 )
    {
      _fup.resetScaFrames();

      // Shift the bits
      memset( data, 0, 4 );
      _fup.addScaFrame( &_trid, SCA_DEV_JTAG, 2, SCA_JTAG_GO, data, _goDelay*8*10 );

      if( get_tdi )
        {
          for( int i=0; i<4; ++i )
            {
              int id = 254;
              _fup.addScaFrame( &id, SCA_DEV_JTAG, 0, JTAG_RD[i], data, 1*10, /*print Readback */get_tdi );
            }
        }

      _fup.uploadScaFrames( _elinknr );

      nbits -= 128;
    }

  _fup.resetScaFrames();

  // Final bits (128 bits or less)
  _ctrlReg &= ~0x007F;
  if( nbits < 128 )
    _ctrlReg |= (nbits & 0x7F);
  mapInt2Bytes( _ctrlReg, data );
  _fup.addScaFrame( &_trid, SCA_DEV_JTAG, 4, SCA_JTAG_WR_CTRL, data );

  // Shift the bits
  memset( data, 0, 4 );
  _fup.addScaFrame( &_trid, SCA_DEV_JTAG, 2, SCA_JTAG_GO, data, _goDelay*8*10 );

  if( get_tdi )
    {
      for( int i=0; i<4; ++i )
        if( nbits > i*32 )
          {
            int id = 254; // Transaction-ID to filter on
            _fup.addScaFrame( &id, SCA_DEV_JTAG, 0, JTAG_RD[i], data, 1*10, /*print Readback */get_tdi );
          }
    }

  _fup.uploadScaFrames( _elinknr );
}

// ----------------------------------------------------------------------------

int JtagPort::setState( state_t s, uint32_t *tms )
{
  // Do nothing for invalid state
  if( s > UPDATE_IR )
    return 0;

  int nbits = 0;
  *tms = 0x0;

  // Reset (from any state in at most 5 clock cycles with TMS=1)
  if( s == TEST_LOGIC_RESET )
    {
      *tms = 0x1F;
      _state = TEST_LOGIC_RESET;
      nbits = 5;
    }

  // Go to the requested TAP state step by step,
  // using the shortest possible path in the state diagram
  while( _state != s )
    {
      if( _state == TEST_LOGIC_RESET && _state != s )
        {
          // TMS=0
          ++nbits; 
          _state = RUN_TEST_IDLE;
        }
      if( _state == RUN_TEST_IDLE && _state != s )
        {
          *tms |= (1 << nbits); // TMS=1
          ++nbits; 
          _state = SELECT_DR_SCAN;
        }
      if( _state == SELECT_DR_SCAN && _state != s )
        {
          if( s < TEST_LOGIC_RESET )
            {
              // TMS=0
              _state = CAPTURE_DR;
            }
          else
            {
              *tms |= (1 << nbits); // TMS=1
              _state = SELECT_IR_SCAN;
            }
          ++nbits;
        }
      if( _state == CAPTURE_DR && _state != s )
        {
          if( s == SHIFT_DR )
            {
              // TMS=0
              _state = SHIFT_DR;
            }
          else
            {
              *tms |= (1 << nbits); // TMS=1
              _state = EXIT1_DR;
            }
          ++nbits;
        }
      if( _state == SHIFT_DR && _state != s )
        {
          *tms |= (1 << nbits); // TMS=1
          ++nbits;
          _state = EXIT1_DR;
        }
      if( _state == EXIT1_DR && _state != s )
        {
          if( s == PAUSE_DR ||
              s == EXIT2_DR )
            {
              // TMS=0
              _state = PAUSE_DR;
            }
          else
            {
              *tms |= (1 << nbits); // TMS=1
              _state = UPDATE_DR;
            }
          ++nbits;
        }
      if( _state == PAUSE_DR && _state != s )
        {
          *tms |= (1 << nbits); // TMS=1
          ++nbits;
          _state = EXIT2_DR;
        }
      if( _state == EXIT2_DR && _state != s )
        {
          if( s == SHIFT_DR ||
              s == EXIT1_DR ||
              s == PAUSE_DR )
            {
              // TMS=0
              _state = SHIFT_DR;
            }
          else
            {
              *tms |= (1 << nbits); // TMS=1
              _state = UPDATE_DR;
            }
          ++nbits;
        }
      if( _state == UPDATE_DR && _state != s )
        {
          if( s == RUN_TEST_IDLE )
            {
              // TMS=0
              _state = RUN_TEST_IDLE;
            }
          else
            {
              *tms |= (1 << nbits); // TMS=1
              _state = SELECT_DR_SCAN;
            }
          ++nbits;
        }
      if( _state == SELECT_IR_SCAN && _state != s )
        {
          if( s != TEST_LOGIC_RESET )
            {
              // TMS=0
              _state = CAPTURE_IR;
            }
          else
            {
              *tms |= (1 << nbits); // TMS=1
              _state = TEST_LOGIC_RESET;
            }
          ++nbits;
        }
      if( _state == CAPTURE_IR && _state != s )
        {
          if( s == SHIFT_IR )
            {
              // TMS=0
              _state = SHIFT_IR;
            }
          else
            {
              *tms |= (1 << nbits); // TMS=1
              _state = EXIT1_IR;
            }
          ++nbits;
        }
      if( _state == SHIFT_IR && _state != s )
        {
          *tms |= (1 << nbits); // TMS=1
          ++nbits;
          _state = EXIT1_IR;
        }
      if( _state == EXIT1_IR && _state != s )
        {
          if( s == PAUSE_IR ||
              s == EXIT2_IR )
            {
              // TMS=0
              _state = PAUSE_IR;
            }
          else
            {
              *tms |= (1 << nbits); // TMS=1
              _state = UPDATE_IR;
            }
          ++nbits;
        }
      if( _state == PAUSE_IR && _state != s )
        {
          *tms |= (1 << nbits); // TMS=1
          ++nbits; 
          _state = EXIT2_IR;
        }
      if( _state == EXIT2_IR && _state != s )
        {
          if( s == SHIFT_IR  ||
              s == EXIT1_IR  ||
              s == PAUSE_IR )
            {
              // TMS=0
              _state = SHIFT_IR;
            }
          else
            {
              *tms |= (1 << nbits); // TMS=1
              _state = UPDATE_IR;
            }
          ++nbits;
        }
      if( _state == UPDATE_IR && _state != s )
        {
          if( s == RUN_TEST_IDLE )
            {
              // TMS=0
              _state = RUN_TEST_IDLE;
            }
          else
            {
              *tms |= (1 << nbits); // TMS=1
              _state = SELECT_DR_SCAN;
            }
          ++nbits;
        }
    }
  return nbits;
}

// ----------------------------------------------------------------------------

void JtagPort::nextState( int tms )
{
  switch( _state )
    {
    case SELECT_DR_SCAN:
      if( tms & 1 )
        _state = SELECT_IR_SCAN;
      else
        _state = CAPTURE_DR;
      break;
    case CAPTURE_DR:
      if( tms & 1 )
        _state = EXIT1_DR;
      else
        _state = SHIFT_DR;
      break;
    case SHIFT_DR:
      if( tms & 1 )
        _state = EXIT1_DR;
      else
        _state = SHIFT_DR;
      break;
    case EXIT1_DR:
      if( tms & 1 )
        _state = UPDATE_DR;
      else
        _state = PAUSE_DR;
      break;
    case PAUSE_DR:
      if( tms & 1 )
        _state = EXIT2_DR;
      else
        _state = PAUSE_DR;
      break;
    case EXIT2_DR:
      if( tms & 1 )
        _state = UPDATE_DR;
      else
        _state = SHIFT_DR;
      break;
    case UPDATE_DR:
      if( tms & 1 )
        _state = SELECT_DR_SCAN;
      else
        _state = RUN_TEST_IDLE;
      break;

    case TEST_LOGIC_RESET:
      if( tms & 1 )
        _state = TEST_LOGIC_RESET;
      else
        _state = RUN_TEST_IDLE;
      break;
    case RUN_TEST_IDLE:
      if( tms & 1 )
        _state = SELECT_DR_SCAN;
      else
        _state = RUN_TEST_IDLE;
      break;

    case SELECT_IR_SCAN:
      if( tms & 1 )
        _state = TEST_LOGIC_RESET;
      else
        _state = CAPTURE_IR;
      break;
    case CAPTURE_IR:
      if( tms & 1 )
        _state = EXIT1_IR;
      else
        _state = SHIFT_IR;
      break;
    case SHIFT_IR:
      if( tms & 1 )
        _state = EXIT1_IR;
      else
        _state = SHIFT_IR;
      break;
    case EXIT1_IR:
      if( tms & 1 )
        _state = UPDATE_IR;
      else
        _state = PAUSE_IR;
      break;
    case PAUSE_IR:
      if( tms & 1 )
        _state = EXIT2_IR;
      else
        _state = PAUSE_IR;
      break;
    case EXIT2_IR:
      if( tms & 1 )
        _state = UPDATE_IR;
      else
        _state = SHIFT_IR;
      break;
    case UPDATE_IR:
      if( tms & 1 )
        _state = SELECT_DR_SCAN;
      else
        _state = RUN_TEST_IDLE;
      break;
    default:
      break;
    }
}

// ----------------------------------------------------------------------------

void JtagPort::addResetTdoTms()
{
  // Reset GBT-SCA JTAG TDO and TMS registers
  u8 data[4];
  memset( data, 0, 4 );
  _fup.addScaFrame( &_trid, SCA_DEV_JTAG, 4, SCA_JTAG_WR_TDO0, data );
  _fup.addScaFrame( &_trid, SCA_DEV_JTAG, 4, SCA_JTAG_WR_TDO1, data );
  _fup.addScaFrame( &_trid, SCA_DEV_JTAG, 4, SCA_JTAG_WR_TDO2, data );
  _fup.addScaFrame( &_trid, SCA_DEV_JTAG, 4, SCA_JTAG_WR_TDO3, data );
  _fup.addScaFrame( &_trid, SCA_DEV_JTAG, 4, SCA_JTAG_WR_TMS0, data );
  _fup.addScaFrame( &_trid, SCA_DEV_JTAG, 4, SCA_JTAG_WR_TMS1, data );
  _fup.addScaFrame( &_trid, SCA_DEV_JTAG, 4, SCA_JTAG_WR_TMS2, data );
  _fup.addScaFrame( &_trid, SCA_DEV_JTAG, 4, SCA_JTAG_WR_TMS3, data );
}

// ----------------------------------------------------------------------------

void JtagPort::mapInt2Bytes( uint32_t i, u8 *bytes )
{
  bytes[2] = (u8) ((i >>  0) & 0xFF);
  bytes[3] = (u8) ((i >>  8) & 0xFF);
  bytes[0] = (u8) ((i >> 16) & 0xFF);
  bytes[1] = (u8) ((i >> 24) & 0xFF);
}

// ----------------------------------------------------------------------------

void JtagPort::displayPercentage( int *percentage,
                                  int  bytes_done,
                                  int  bytes_todo )
{
  // Display percentage done (if its value changes)
  int p = *percentage;
  double pf = ((double) 100.0*bytes_done)/(double) bytes_todo;
  if( (int) pf != p )
    {
      p = (int) pf;
      // Overwrite currently displayed percentage value
      std::cout << "\b\b\b\b\b    \b\b\b\b" << std::setw(3) << p << "% ";
      std::cout.flush();
      *percentage = p;
    }
}

// ----------------------------------------------------------------------------

} // namespace HenksJtag
