#include <string.h> // for memset()
#include <unistd.h> // for usleep()
#include <time.h>   // for nanosleep()

#include <HenksJtag/FlxUpload.h>


#include <iostream>
#include <iomanip>
#include <sstream> // for ostringstream

#include <LogIt.h>

#include <algorithm>

using namespace std;
using namespace Sca;

namespace HenksJtag
{

// ----------------------------------------------------------------------------

FlxUpload::FlxUpload( Sca::Sca& sca, unsigned int maxGroupSize )
  :
        m_sca(sca),
        m_synchronousService(sca.synchronousService()),
        m_maxGroupSize(maxGroupSize)
{
    m_groupedRequests.reserve(256);
}

FlxUpload::~FlxUpload()
{
}


bool FlxUpload::uploadScaFrame( int elink,
                                int *trid, int chan, int len, int cmd,
                                u8  *data )
{
    if (m_groupedRequests.size()>1)
    {
        LOG(Log::WRN) << "This seems wrong: uploadScaFrame() called but there are buffered frames!";
    }
    Request request( chan, cmd, data, len);
    Reply reply = m_synchronousService.sendAndWaitReply(request);
    throwIfScaReplyError(reply);
    return true; // error will be thrown
}

// ----------------------------------------------------------------------------

void FlxUpload::resetScaFrames()
{
  m_groupedRequests.clear();
  m_groupedRequests.reserve(256);
}

// ----------------------------------------------------------------------------

bool FlxUpload::addScaFrame( int *trid, int chan, int len, int cmd, u8 *data, unsigned int delay, bool storeReadBack )
{
    Request request( chan, cmd, data, len);
    request.setTrailingPause(2);
    if (storeReadBack)
    {
        // here we really have to flush out ...
        LOG(Log::TRC) << "Flushing queue because readback was requested";
        uploadScaFrames();
        //
        Reply reply = m_synchronousService.sendAndWaitReply(request);
        m_readBackFrames.push_back(reply);
        throwIfScaReplyError(reply);
        LOG(Log::INF) << "Readback frame was:" << reply.toString();
    }
    else
    {
        if (m_groupedRequests.size() >= m_maxGroupSize)
            uploadScaFrames(0);
        m_groupedRequests.push_back(request);
        LOG(Log::TRC) << "Pushed back this frame:" << request.toString();
    }
    return true;
}

bool FlxUpload::uploadScaFrames     ( int elink )
{
    if (m_groupedRequests.size() > 0)
    {
        LOG(Log::TRC) << "Will flush " << m_groupedRequests.size() << " frames";
        std::vector<Reply> replies = m_synchronousService.sendAndWaitReply( m_groupedRequests );
        std::for_each( replies.begin(), replies.end(), [](Reply &r){ throwIfScaReplyError(r); } );
        resetScaFrames();
    }
    return true;
}

}
