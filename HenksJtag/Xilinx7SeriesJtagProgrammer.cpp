/*
 * Xilinx7SeriesJtagProgrammer.cpp
 *
 *  Created on: 18 Jun 2018
 *      Author: pnikiel
 */

#include <HenksJtag/Xilinx7SeriesJtagProgrammer.h>
#include <HenksJtag/BitfileReader.h>

using namespace std;

namespace HenksJtag
{

// Xilinx VIRTEX7 JTAG instructions
#define V7_IRLEN        6
#define V7_IDCODE_LEN   32
#define V7_CFG_OUT      0x04
#define V7_CFG_IN       0x05
#define V7_IDCODE       0x09
#define V7_JPROGRAM     0x0B
#define V7_JSTART       0x0C
#define V7_JSHUTDOWN    0x0D
#define V7_ISC_ENABLE   0x10
#define V7_ISC_PROGRAM  0x11
#define V7_ISC_DISABLE  0x16
#define V7_BYPASS       0x3F

const string V7_STATBITS_STR[] = {
        "CRC ERROR                     ",
        "DECRYPTOR ENABLE              ",
        "PLL LOCK STATUS               ",
        "DCI MATCH STATUS              ",
        "END OF STARTUP (EOS) STATUS   ",
        "GTS_CFG_B STATUS              ",
        "GWE STATUS                    ",
        "GHIGH STATUS                  ",
        "MODE PIN M[0]                 ",
        "MODE PIN M[1]                 ",
        "MODE PIN M[2]                 ",
        "INIT_B INTERNAL SIGNAL STATUS ",
        "INIT_B PIN                    ",
        "DONE INTERNAL SIGNAL STATUS   ",
        "DONE PIN                      ",
        "IDCODE ERROR                  ",
        "SECURITY ERROR                ",
        "SYSTEM MON OVERTEMP ALARM STAT",
        "CFG STARTUP STATEMACHINE PHASE",
        "CFG STARTUP STATEMACHINE PHASE",
        "CFG STARTUP STATEMACHINE PHASE",
        "RESERVED                      ",
        "RESERVED                      ",
        "RESERVED                      ",
        "RESERVED                      ",
        "CFG BUS WIDTH DETECTION       ",
        "CFG BUS WIDTH DETECTION       ",
        "HMAC ERROR                    ",
        "PUDC_B PIN                    ",
        "BAD PACKET ERROR              ",
        "CFGBVS PIN                    ",
        "RESERVED                      "
};

Xilinx7SeriesJtagProgrammer::Xilinx7SeriesJtagProgrammer(Sca::Sca& sca, unsigned int freqMhz, unsigned int maxGroupSize):
        m_sca(sca),
        m_flxUpload(m_sca, maxGroupSize),
        jtag(m_flxUpload, /*elink number - this is fictuitious just to keep the API*/ 0),
        m_freqMhz(freqMhz)
{

}

Xilinx7SeriesJtagProgrammer::~Xilinx7SeriesJtagProgrammer()
{
}

//! Will return false if the programming result was not OK,
//! Will throw in case of an exceptional situation
bool Xilinx7SeriesJtagProgrammer::programFromBitFile(
        const std::string& filename,
        bool               byte_rev)
{
    BitfileReader bitfile;
    bitfile.setFile( filename, /*skip_data*/ false );
    if( byte_rev )
    {
        // Note: alternatively set GBT-SCA JTAG channel to shift MSB-to-LSB
        // and change the byte-ordering in JtagPort::shiftDr(),
        // although that may have implications for the TMS bits in
        // gotoState() and the instruction in shiftIr() (to be tested)

        //cout << "NB: Reversing bits-in-bytes" << endl;
        bitfile.reverseBytes();
        //display_bitfile_info( bitfile );
    }

    if (!bitfile.valid())
        throw std::runtime_error("TODO better exception");
    return this->programFromBitFileReader(bitfile);
}

bool Xilinx7SeriesJtagProgrammer::programFromBlob(const std::string& blob, bool reverseBytes)
{
    BitfileReader bitfile;
    bitfile.setBlob( blob );
    if( reverseBytes )
    {
        // Note: alternatively set GBT-SCA JTAG channel to shift MSB-to-LSB
        // and change the byte-ordering in JtagPort::shiftDr(),
        // although that may have implications for the TMS bits in
        // gotoState() and the instruction in shiftIr() (to be tested)

        //cout << "NB: Reversing bits-in-bytes" << endl;
        bitfile.reverseBytes();
        //display_bitfile_info( bitfile );
    }

    if (!bitfile.valid())
        throw std::runtime_error("TODO better exception");
    return this->programFromBitFileReader(bitfile);

}

bool Xilinx7SeriesJtagProgrammer::programFromBitFileReader(BitfileReader& reader)
{
    jtag.enable( m_freqMhz );

    jtag.setIrLen( V7_IRLEN ); // Assuming a single device in the JTAG chain

    // Go to a defined state
    jtag.gotoState( JtagPort::TEST_LOGIC_RESET );

    jtag.showProgress( true );
    jtag.shiftIr( V7_JPROGRAM );
    jtag.gotoState( JtagPort::TEST_LOGIC_RESET );
    jtag.gotoState( JtagPort::RUN_TEST_IDLE );
    jtag.shift( 10000*m_freqMhz );
    jtag.shiftIr( V7_CFG_IN );
    jtag.shiftDr( reader.nbytes()*8, reader.data() );
    jtag.shiftIr( V7_JSTART );
    jtag.shift( 2000*m_freqMhz );
    jtag.gotoState( JtagPort::TEST_LOGIC_RESET );

    m_flxUpload.clearReadBack();

    jtag.gotoState( JtagPort::SHIFT_IR );
    jtag.shift( V7_IRLEN, /* get_tdi*/ true );
    jtag.gotoState( JtagPort::TEST_LOGIC_RESET );

    std::vector<Sca::Reply> irlen_reply = m_flxUpload.getReadBackFrames();
    if (irlen_reply.size() != 1)
    {
        scasw_throw_runtime_error_with_origin("logic issue: expected just one reply, got "+boost::lexical_cast<std::string>(irlen_reply.size()));
    }

    if (irlen_reply[0][6] == 0x35)
        return true; // programming was OK
    else
    {
        LOG(Log::ERR) << "FPGA programming likely failed, reply frame: " << irlen_reply[0].toString();
        return false;
    }

}

uint32_t Xilinx7SeriesJtagProgrammer::readId()
{
    jtag.enable( m_freqMhz );
    jtag.setIrLen( V7_IRLEN ); // Assuming a single device in the JTAG chain    

    jtag.shiftIr( V7_IDCODE );
     m_flxUpload.clearReadBack();
    jtag.shiftDr( V7_IDCODE_LEN, /*tdo*/nullptr, /*get_tdi*/ true );
    jtag.gotoState( JtagPort::TEST_LOGIC_RESET );

    std::vector<Sca::Reply> replies = m_flxUpload.getReadBackFrames();

    if (replies.size() != 1)
    {
        scasw_throw_runtime_error_with_origin("logic issue: expected just one reply, got "+boost::lexical_cast<std::string>(replies.size()));
    }

    uint32_t code = replies[0][5] << 24 | replies[0][4] << 16 | replies[0][7] << 8 | replies[0][6] ;


    return code;


}


std::string Xilinx7SeriesJtagProgrammer::idCodeToString(uint32_t id)
{
    std::stringstream output;
    output.fill('0');
    output.width(6);
    output << std::hex << id;
    // Xilinx 7-series FPGA family member IDs
    id = (id & 0x0FFFF000) >> 12;
    if( id == 0x3622 || id == 0x3620 || id == 0x37C4 ||
            id == 0x362F || id == 0x37C8 || id == 0x37C7 )
        output << " (Spartan-7)";
    else if( id == 0x37C3 || id == 0x362E || id == 0x37C2 ||
            id == 0x362D || id == 0x362C || id == 0x3632 ||
            id == 0x3631 || id == 0x3636 )
        output << " (Artix-7)";
    else if( id == 0x3647 || id == 0x364C || id == 0x3651 ||
            id == 0x3747 || id == 0x3656 || id == 0x3752 ||
            id == 0x3751 )
        output << " (Kintex-7)";
    else if( id == 0x3671 || id == 0x36B3 || id == 0x3667 ||
            id == 0x3682 || id == 0x3687 || id == 0x3692 ||
            id == 0x3691 || id == 0x3696 || id == 0x36D5 ||
            id == 0x36D9 || id == 0x36DB )
        output << " (Virtex-7)";
    else if (id == 0x0000 || id == 0xffff)
        output << " (no device connected?)";
    else
        output << " (<unknown>)";
    return output.str();
}


} /* namespace HenksJtag */

