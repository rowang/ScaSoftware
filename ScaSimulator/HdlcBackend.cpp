#include <ScaSimulator/HdlcBackend.h>
#include <ScaSimulator/ScaChannel.h>
#include <ScaSimulator/ADC.h>
#include <ScaSimulator/SPI.h>
#include <ScaSimulator/GPIO.h>
#include <ScaSimulator/DAC.h>
#include <ScaSimulator/JTAG.h>
#include <ScaSimulator/I2C.h>

#include <ScaCommon/except.h>
#include <ScaCommon/ScaSwLogComponents.h>

#include <Sca/Defs.h>

#include <LogIt.h>

#include <thread>
#include <mutex>

#include <boost/bind.hpp>
#include <boost/lexical_cast.hpp>

#include <iostream>
#include <unistd.h>  // this is for rand()

using Sca::LogComponentLevels;

namespace ScaSimulator
{

    HdlcBackend::HdlcBackend (const std::string& specificAddress):
	m_specificAddress(specificAddress),
	m_scaId(boost::lexical_cast<unsigned int>(specificAddress)),
	m_channelEnabledMask (0), // reset condition, all channels are disabled,
	m_keepRunning(true)

    {
	// create ADC
	auto adc = new ADC (Sca::Constants::ChannelIds::ADC, this);
	this->connectChannel( adc->channelId(), adc );
	// create SPI
	auto spi = new SPI (Sca::Constants::ChannelIds::SPI, this);
	this->connectChannel( spi->channelId(), spi );
	// create GPIO
	auto gpio = new GPIO (Sca::Constants::ChannelIds::GPIO, this);
	this->connectChannel( gpio->channelId(), gpio );
	// create DAC
	auto dac = new DAC (Sca::Constants::ChannelIds::DAC, this);
	this->connectChannel( dac->channelId(), dac );
	// create JTAG
	auto jtag = new JTAG (Sca::Constants::ChannelIds::JTAG, this);
	this->connectChannel( jtag->channelId(), jtag );
	// create I2C
	auto i2c0 = new I2C (Sca::Constants::ChannelIds::I2C0, this);
	this->connectChannel( i2c0->channelId(), i2c0 );
	auto i2c1 = new I2C (Sca::Constants::ChannelIds::I2C1, this);
	this->connectChannel( i2c1->channelId(), i2c1 );
	auto i2c2 = new I2C (Sca::Constants::ChannelIds::I2C2, this);
	this->connectChannel( i2c2->channelId(), i2c2 );
	auto i2c3 = new I2C (Sca::Constants::ChannelIds::I2C3, this);
	this->connectChannel( i2c3->channelId(), i2c3 );
	auto i2c4 = new I2C (Sca::Constants::ChannelIds::I2C4, this);
	this->connectChannel( i2c4->channelId(), i2c4 );
	auto i2c5 = new I2C (Sca::Constants::ChannelIds::I2C5, this);
	this->connectChannel( i2c5->channelId(), i2c5 );
	auto i2c6 = new I2C (Sca::Constants::ChannelIds::I2C6, this);
	this->connectChannel( i2c6->channelId(), i2c6 );
	auto i2c7 = new I2C (Sca::Constants::ChannelIds::I2C7, this);
	this->connectChannel( i2c7->channelId(), i2c7 );
	auto i2c8 = new I2C (Sca::Constants::ChannelIds::I2C8, this);
	this->connectChannel( i2c8->channelId(), i2c8 );
	auto i2c9 = new I2C (Sca::Constants::ChannelIds::I2C9, this);
	this->connectChannel( i2c9->channelId(), i2c9 );
	auto i2cA = new I2C (Sca::Constants::ChannelIds::I2CA, this);
	this->connectChannel( i2cA->channelId(), i2cA );
	auto i2cB = new I2C (Sca::Constants::ChannelIds::I2CB, this);
	this->connectChannel( i2cB->channelId(), i2cB );
	auto i2cC = new I2C (Sca::Constants::ChannelIds::I2CC, this);
	this->connectChannel( i2cC->channelId(), i2cC );
	auto i2cD = new I2C (Sca::Constants::ChannelIds::I2CD, this);
	this->connectChannel( i2cD->channelId(), i2cD );
	auto i2cE = new I2C (Sca::Constants::ChannelIds::I2CE, this);
	this->connectChannel( i2cE->channelId(), i2cE );
	auto i2cF = new I2C (Sca::Constants::ChannelIds::I2CF, this);
	this->connectChannel( i2cF->channelId(), i2cF );

	m_simThread = std::move(std::thread(boost::bind(&HdlcBackend::tickThread, this)));
    }

HdlcBackend::~HdlcBackend ()
{
    m_keepRunning = false;
    m_simThread.join();
}

    void HdlcBackend::send (const Hdlc::Payload &request )
    {
	LOG(Log::TRC, LogComponentLevels::sim()) << "simulated SCA: obtained frame!" << request.toString() ;
	m_statistician.onFrameSent();
	Sca::Request frame( request );
	// handle NODE channel here, as it is at whole SCA scope, and all remaining through registered handlers
	if (frame.channelId() == Sca::Constants::ChannelIds::NODE)
	{
	    handleNodeRequest (frame);
	}
	else if (m_channelMap.count( frame.channelId() ) > 0)
	{
	    if (! (m_channelEnabledMask & ( 1 << frame.channelId() ) ))
	    {
		Sca::Reply channelDisabled(
		    frame.channelId(),
		    Sca::Constants::Errors::CHANNEL_DISABLED,
		    {} );
		channelDisabled.assignTransactionId( frame.transactionId() );
		boost::chrono::microseconds deadTime (100);
		this->storeReply( channelDisabled, deadTime );
		
	    }
	    else
	    {
		ScaChannel *channel = m_channelMap[ frame.channelId() ];
		if (channel->isBusy())
		{
		    Sca::Reply channelBusy(
			frame.channelId(),
			Sca::Constants::Errors::CHANNEL_BUSY,
			{} );
		    channelBusy.assignTransactionId( frame.transactionId() );
		    boost::chrono::microseconds deadTime (100);
		    this->storeReply( channelBusy, deadTime );
		}
		else
		    channel->onReceive( request );
	    }
	}
	else
	{
	    // TODO: generate SCA error frame that this channel is not registered?
            std::cout << "this channel is not registered -- ignoring this request (channel id was: " << (unsigned int)frame.channelId() << ")" << std::endl;
	}
    }

void HdlcBackend::send (
              const std::vector<Hdlc::Payload> &requests,
              const std::vector<unsigned int> times
          )
{
    // the implementation for the SCA simulator is a bit naive. at least yet.
    for (unsigned int i=0; i<requests.size(); ++i)
    {
        this->send( requests[i] );
        usleep( times[i] );

    }

}

void HdlcBackend::connectChannel(unsigned char channelId, ScaChannel* channel)
{
	m_channelMap[channelId] = channel ;
}

    void HdlcBackend::storeReply(
	const Hdlc::Payload& reply,
	boost::chrono::microseconds t)
    {
        std::lock_guard<decltype(m_accessLock)> lock (m_accessLock);
        m_pendingReplies.push_back( TimeBoundReply(reply, boost::chrono::steady_clock::now() + t ));
    }



    void HdlcBackend::tick()
    {
        std::vector<Hdlc::Payload> repliesToSendOut;
        repliesToSendOut.reserve(32); // approximately number of SCA components
        {
            std::lock_guard<decltype(m_accessLock)> lock (m_accessLock);
            auto now = boost::chrono::steady_clock::now();
            for ( auto it = m_pendingReplies.begin(); it!=m_pendingReplies.end(); it++)
            {
                if ( now > it->when)
                {
                    LOG(Log::TRC, LogComponentLevels::sim()) << "flushing reply: " << it->payload.toString();
                    repliesToSendOut.push_back( it->payload );
                    it = m_pendingReplies.erase (it);
                    m_statistician.onFrameReceived();
                }
            }
        }
        for (Hdlc::Payload& payload : repliesToSendOut)
            m_replyCame(payload);
    }

    void HdlcBackend::tickThread()
    {
        while (m_keepRunning)
        {
            usleep(100);
            this->tick();
        }
    }

    void HdlcBackend::handleNodeRequest(const Hdlc::Payload &payload )
    {
	Sca::Request frame( payload );
	
	unsigned int cmd = frame.command();
	unsigned int whichByte;
	bool isWrite;

	using Sca::Constants::Commands;
	
	switch(cmd)
	{
	case Commands::NC_W_CRB:
	{
	    whichByte = 0;
	    isWrite = 1;
	}
	break;
	case Commands::NC_R_CRB:
	{
	    whichByte = 0;
	    isWrite = 0;
	}
	break;
	case Commands::NC_W_CRC:
	{
	    whichByte = 1;
	    isWrite = 1;
	}
	break;
	case Commands::NC_R_CRC:
	{
	    whichByte = 1;
	    isWrite = 0;
	}
	break;
	case Commands::NC_W_CRD:
	{
	    whichByte = 2;
	    isWrite = 1;
	}
	break;
	case Commands::NC_R_CRD:
	{
	    whichByte = 2;
	    isWrite = 0;
	}
	break;

       
	default:
	    throw std::runtime_error("This NODE request is not implemented "+frame.toString());
	    
	}
	if (isWrite)
	{
	    if (frame.size() < 5)
		throw std::runtime_error("Frame too small for the request.");
	    
	    
	    uint32_t mask = m_channelEnabledMask;

	    // clean 8-bit region which will be replaced by the new, coming one
	    mask &= ~( 0xff << (whichByte * 8) );
	    // replace with the new one
	    mask |= ((uint32_t)payload[5] ) << (whichByte * 8);
	    m_channelEnabledMask = mask;
	    LOG(Log::INF, LogComponentLevels::sim()) << "Channel enabled control: new mask is " << std::hex << mask << std::dec;
	    

	    Sca::Reply reply(
		frame.channelId(),
		0 /* NO ERROR */,
		{} );
	    reply.assignTransactionId( frame.transactionId() );
	    boost::chrono::microseconds deadTime (100);
	    this->storeReply( reply, deadTime );
	}
	else
	{
	    Sca::Reply reply(
		frame.channelId(),
		0 /* NO ERROR */,
		{ 0, static_cast<uint8_t>(m_channelEnabledMask >> (whichByte*8)) } );
	    reply.assignTransactionId( frame.transactionId() );
	    boost::chrono::microseconds deadTime (100);
	    this->storeReply( reply, deadTime );
	    

	    
	}
	
	
	

	
    }

    }
