/*
 * SPI.cpp
 *
 *  Created on: Nov 3, 2016
 *      Author: pmoschov
 */


#include <ScaSimulator/SPI.h>

#include <Sca/Defs.h>

#include <stdlib.h>

#include <iostream>

namespace ScaSimulator
{

SPI::SPI(unsigned char channelId, HdlcBackend *myBackend) :
		ScaChannel(channelId, myBackend)
{
}

SPI::~SPI()
{
}

    void SPI::onReceive(const Sca::Request &request)
    {
	using Sca::Constants::Commands;

	switch( request.command() )
	{
	case Commands::SPI_GO:
	{
	    uint32_t spiValue = rand();
	    Sca::Reply reply (request.channelId(), /*err*/ 0x00, /*data*/ { (uint8_t)(spiValue), (uint8_t)((spiValue)>>8),  (uint8_t)((spiValue)>>16), (uint8_t)((spiValue)>>24) } );
	    unsigned int delay = 1 + rand()%2;

	    boost::chrono::milliseconds processingTime (delay);
	    this->sendReply( request, reply, processingTime );
	    this->keepBusyFor( processingTime );
	}
	break;
	case Commands::SPI_W_SS:
	{
	    Sca::Reply reply (request.channelId(), /*err*/ 0x00, /*data*/ {} );
	    unsigned int delay = 1 + rand()%2;

	    boost::chrono::milliseconds processingTime (delay);
	    this->sendReply( request, reply, processingTime );
	    this->keepBusyFor( processingTime );
	}
	break;
    case Commands::SPI_R_CTRL:
    {
        Sca::Reply reply (request.channelId(), /*err*/ 0x00, /*data*/ { (uint8_t)m_divider, (uint8_t)(m_divider >> 8), 0, 0} );
        unsigned int delay = 1 + rand()%2;

        boost::chrono::milliseconds processingTime (delay);
        this->sendReply( request, reply, processingTime );
        this->keepBusyFor( processingTime );
    }
    break;
    case Commands::SPI_W_CTRL:
    {
        m_divider = 10;
        Sca::Reply reply (request.channelId(), /*err*/ 0x00, /*data*/ {} );
        unsigned int delay = 1 + rand()%2;

        boost::chrono::milliseconds processingTime (delay);
        this->sendReply( request, reply, processingTime );
        this->keepBusyFor( processingTime );
    }
    break;
	case Commands::SPI_R_FREQ:
		{
		    Sca::Reply reply (request.channelId(), /*err*/ 0x00, /*data*/ { (uint8_t)m_divider, (uint8_t)(m_divider >> 8), 0, 0} );
		    unsigned int delay = 1 + rand()%2;

		    boost::chrono::milliseconds processingTime (delay);
		    this->sendReply( request, reply, processingTime );
		    this->keepBusyFor( processingTime );
		}
	break;
	case Commands::SPI_W_FREQ:
		{
			m_divider = 10;
		    Sca::Reply reply (request.channelId(), /*err*/ 0x00, /*data*/ {} );
		    unsigned int delay = 1 + rand()%2;

		    boost::chrono::milliseconds processingTime (delay);
		    this->sendReply( request, reply, processingTime );
		    this->keepBusyFor( processingTime );
		}
	break;
	case Commands::SPI_W_MOSI0:
		{
			uint32_t spiValue = rand();
			Sca::Reply reply (request.channelId(), /*err*/ 0x00, /*data*/ { (uint8_t)(spiValue), (uint8_t)((spiValue)>>8),  (uint8_t)((spiValue)>>16), (uint8_t)((spiValue)>>24) } );
		    unsigned int delay = 1 + rand()%2;

		    boost::chrono::milliseconds processingTime (delay);
		    this->sendReply( request, reply, processingTime );
		    this->keepBusyFor( processingTime );
		}
	break;
	case Commands::SPI_W_MOSI1:
		{
			uint32_t spiValue = rand();
			Sca::Reply reply (request.channelId(), /*err*/ 0x00, /*data*/ { (uint8_t)(spiValue), (uint8_t)((spiValue)>>8),  (uint8_t)((spiValue)>>16), (uint8_t)((spiValue)>>24) } );
		    unsigned int delay = 1 + rand()%2;

		    boost::chrono::milliseconds processingTime (delay);
		    this->sendReply( request, reply, processingTime );
		    this->keepBusyFor( processingTime );
		}
	break;
	case Commands::SPI_W_MOSI2:
		{
			uint32_t spiValue = rand();
			Sca::Reply reply (request.channelId(), /*err*/ 0x00, /*data*/ { (uint8_t)(spiValue), (uint8_t)((spiValue)>>8),  (uint8_t)((spiValue)>>16), (uint8_t)((spiValue)>>24) } );
		    unsigned int delay = 1 + rand()%2;

		    boost::chrono::milliseconds processingTime (delay);
		    this->sendReply( request, reply, processingTime );
		    this->keepBusyFor( processingTime );
		}
	break;
	case Commands::SPI_W_MOSI3:
		{
			uint32_t spiValue = rand();
			Sca::Reply reply (request.channelId(), /*err*/ 0x00, /*data*/ { (uint8_t)(spiValue), (uint8_t)((spiValue)>>8),  (uint8_t)((spiValue)>>16), (uint8_t)((spiValue)>>24) } );
		    unsigned int delay = 1 + rand()%2;

		    boost::chrono::milliseconds processingTime (delay);
		    this->sendReply( request, reply, processingTime );
		    this->keepBusyFor( processingTime );
		}
	break;
	}


    }



}
