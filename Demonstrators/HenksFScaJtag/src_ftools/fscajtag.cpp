#include <fstream>
#include <iostream>
#include <iomanip>
#include <functional>
using namespace std;
#include <unistd.h>

#include <HenksJtag/FlxUpload.h>
#include <HenksJtag/BitfileReader.h>
#include <HenksJtag/JtagPort.h>
#include <HenksJtag/Xilinx7SeriesJtagProgrammer.h>

#include <string.h>

#include <Hdlc/BackendFactory.h>
#include <boost/program_options.hpp>

#include <VeryHipEvents/EventRecorder.h>
using VeryHipEvents::Recorder;

#include <DemonstratorCommons.h>

struct Config
{
    std::string          address;
    std::string          bitfile;
    bool                 debug;
    unsigned int         maxGroupSize;
    bool                 readId;
    bool                 dump;
  double                 jtagFrequency;
};

Config parseProgramOptions(int argc, char* argv[])
{
    using namespace boost::program_options;
    using namespace ScaSwDemonstrators;
    Config config;
    options_description options;
    std::string logLevelStr;
    options.add_options()
                    ("help,h",                                                                                     "show help")
                    ("address,a",        value<std::string>(&config.address)->default_value("sca-simulator://1"),  helpForAddress )
                    ("trace_level,t",    value<std::string>(&logLevelStr)->default_value("INF"),                   "Trace level, one of: ERR,WRN,INF,DBG,TRC")
                    ("bitfile",          value<std::string>(&config.bitfile),                                      "Path to the bitfile")
                    ("max_group_size,g", value<unsigned int>(&config.maxGroupSize)->default_value(1),              "Max size of a request group, pick from range of [1-253]" )
                    ("jtag_freq,j",      value<double>(&config.jtagFrequency)->default_value(20.0),                "JTAG TCK frequency, accepted values: 1|2|5|10|20 MHz" )
                    ("read_id,i",        bool_switch(&config.readId),                                              "Just read chip ID and exit"  )
                    ("dump,d",           bool_switch(&config.dump),                                                "Dump transactions into dump.xml")
                    ;
    variables_map vm;
    store( parse_command_line (argc, argv, options), vm );
    notify (vm);
    if (vm.count("help"))
    {
        std::cout << options << std::endl;
        exit(1);
    }
    initializeLogging(logLevelStr);
    if (config.maxGroupSize < 1 || config.maxGroupSize > 253)
    {
        std::cout << "Max group size out of range, pick a value from 1 (no grouping) up to 253" << std::endl;
        exit(1);
    }

    return config;
}

class RaiiWrapper
{
public:
    RaiiWrapper(const std::function<void()>& f): m_onDelete(f) {}
    ~RaiiWrapper() { m_onDelete (); }
private:
    std::function<void()> m_onDelete;
};

int main( int argc, char *argv[] )
{

    Config config = parseProgramOptions(argc, argv);
    if (config.dump)
    {
        Recorder::initialize(1E6, 1E8);
    }
    RaiiWrapper dumpOnExit( [config](){
        if (config.dump)
        {
            LOG(Log::INF) << "Hang tight, we're dumping the trace now.";
            Recorder::dump("dump.xml");
        }
        });
    Sca::Sca sca (config.address);

    HenksJtag::Xilinx7SeriesJtagProgrammer programmer(sca, config.jtagFrequency, config.maxGroupSize);
    if (config.readId)
    {
        uint32_t id = programmer.readId();
        LOG(Log::INF) << "the ID is: " << std::hex << id;
        LOG(Log::INF) << "which is: " << HenksJtag::Xilinx7SeriesJtagProgrammer::idCodeToString(id);
    }
        else
            programmer.programFromBitFile(config.bitfile, /*byte_rev*/ true);


    return 0;
}

