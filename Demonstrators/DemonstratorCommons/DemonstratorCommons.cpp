/*
 * DemonstratorCommons.cpp
 *
 *  Created on: 13 Dec 2018
 *      Author: pnikiel
 */

#include "DemonstratorCommons.h"

#include <ScaCommon/ScaSwLogComponents.h>
#include <ScaCommon/except.h>

namespace ScaSwDemonstrators
{


void initializeLogging (const std::string& logLevelStr)
{
    Log::LOG_LEVEL       logLevel;
    if (! Log::logLevelFromString( logLevelStr, logLevel ) )
        THROW_WITH_ORIGIN(std::runtime_error, "Log level not recognized: '"+logLevelStr+"'");
    Log::initializeLogging( logLevel );
    Sca::LogComponentLevels::initializeScaSw(logLevel );
}

}
