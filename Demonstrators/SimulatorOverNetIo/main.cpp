#include <iostream>
#include <stdlib.h>
#include <signal.h>
#include <algorithm>

#include <boost/program_options.hpp>
#include <boost/thread.hpp>

#include <felixbase/client.hpp>
#include <netio/netio.hpp>
#include <hdlc_coder/hdlc.hpp>

#include <ScaSimulator/HdlcBackend.h>
#include <LogIt.h>

#include <DemonstratorCommons.h>

#include <iterator>
#include <random>

typedef uint64_t ElinkId;

const unsigned int NETIO_FRAME_HDLC_START_OFFSET = 8;  // HDLC frame shall be placed starting from this index
const unsigned int HDLC_FRAME_PAYLOAD_OFFSET = 2;   // HDLC payload offset in HDLC frame
const unsigned int HDLC_FRAME_TRAILER_SIZE = 2;   // HDLC trailer size in HDLC frame

struct Config
{
    uint16_t             requestsPort;
    uint16_t             repliesPort;
    ElinkId              elinkId;
    bool                 doNoiseReplies;
    unsigned int         noiseRepliesInterval;
    bool                 doLoseReplies;
    double               requestsLossProbability;
};

Config parseProgramOptions(int argc, char* argv[])
{
    using namespace boost::program_options;
    using namespace ScaSwDemonstrators;
    Config config;
    options_description options;
    std::string logLevelStr;
    options.add_options()
            ("help,h", "show help")
            ("trace_level,t",   value<std::string>(&logLevelStr)->default_value("INF"),               "Trace level, one of: ERR,WRN,INF,DBG,TRC")
            ("requests_port,x", value<uint16_t>(&config.requestsPort)->default_value(29000),          "TCP/IP port on which netio will listen for requests")
            ("replies_port,y",  value<uint16_t>(&config.repliesPort)->default_value(29001),           "TCP/IP port to which netio will publish the replies")
            ("elink_id,e",      value<ElinkId>(&config.elinkId)->default_value(0x01),                 "Elink-id at which this simulated SCA listens")
            ("noise_replies",   bool_switch(&config.doNoiseReplies),                                  "If selected, random traffic will be injected into replies. Simulates unconnected elink/fibre/ broken firmware. ")
            ("noise_replies_interval", value<unsigned int>(&config.noiseRepliesInterval)->default_value(1000), "The delay between consecutive transmission of noise frames in microseconds")
            ("requests_loss",   bool_switch(&config.doLoseReplies),                                   "If selected, certain requests will be ignored (simulating loss). The probability is chosen by --requests_loss_prob")
            ("requests_loss_prob", value<double>(&config.requestsLossProbability)->default_value(1E-3),"Chance of losing a request. 1.0 corresponds to full loss, 0.0 to no loss. Valid only when --requests_loss chosen")
            ;
    variables_map vm;
    store( parse_command_line (argc, argv, options), vm );
    notify (vm);
    if (vm.count("help"))
    {
        std::cout << options << std::endl;
        exit(1);
    }
    initializeLogging(logLevelStr);
    return config;
}

bool simulateElinkDisruption = false;

void signalEnableDisruption(int)
{
    simulateElinkDisruption = !simulateElinkDisruption;
    LOG(Log::INF) << "Simulating disruption=" << simulateElinkDisruption << "; Send SIGUSR1 to disable";
}

void noiseReplies(netio::publish_socket* socket, netio::tag* tag, unsigned int noiseIntervalUs)
{
    std::vector<unsigned char> data (256,0);
    while(1)
    {
        // this_size will be the size of next random package, between 8-32 B
        unsigned int this_size = 8 + rand() % 24;
        std::generate_n(data.begin(), this_size, rand);
        netio::message msg (&data[0], this_size);
        LOG(Log::TRC) << "Emitting noise frame, size=" << this_size;
        socket->publish(*tag, msg);
        usleep(noiseIntervalUs);
    }
}

int main(int argc, char* argv[] )
{
    std::default_random_engine generator;
    std::uniform_real_distribution<double> distribution;

    Config config = parseProgramOptions( argc, argv);
	LOG(Log::INF) << "initialized logging";

	signal(SIGUSR1, signalEnableDisruption );

	netio::context context("posix");

	boost::thread netio_thread( [&context](){
		context.event_loop()->run_forever();

	});

	/* NOTE for the id. It doesn't matter as long as we keep one simulated SCA in this program.
	 * It doesn't get exposed externally either.
	 */
	ScaSimulator::HdlcBackend simulator ("1");

	netio::publish_socket reply_socket( &context, config.repliesPort );
	netio::tag publish_tag ( config.elinkId );

	std::unique_ptr<std::thread> replyNoiseThread;
	if (config.doNoiseReplies)
	{
	    replyNoiseThread = std::unique_ptr<std::thread>(new std::thread(noiseReplies, &reply_socket, &publish_tag, config.noiseRepliesInterval));
	}

	uint64_t seqnr = 0;

	// This is a "lambda" type implementation which fits expected std::function handler
	Hdlc::Backend::ReceiveCallBack receiveCallback ( [&reply_socket,&publish_tag,&seqnr,&config](const Hdlc::Payload& payload)
        {
            // HDLC framing - Paris

            if (seqnr >= 7) seqnr = 0; else seqnr++;

            int addr = 0;


            std::vector<uint8_t> hdlcFrame (HDLC_FRAME_PAYLOAD_OFFSET + payload.size() + HDLC_FRAME_TRAILER_SIZE);

            felix::hdlc::encode_hdlc_msg_header(&hdlcFrame[0], addr, seqnr);
            std::copy(payload.cbegin(), payload.cend(), &hdlcFrame[HDLC_FRAME_PAYLOAD_OFFSET]);
            felix::hdlc::encode_hdlc_trailer(
                    &hdlcFrame[HDLC_FRAME_PAYLOAD_OFFSET+payload.size()],
                    &hdlcFrame[0],
                    payload.size() + HDLC_FRAME_PAYLOAD_OFFSET);

            LOG(Log::TRC) << "The size of the HDLC frame is: " << hdlcFrame.size();

            felix::base::FromFELIXHeader header;
            header.elinkid = config.elinkId;
            header.gbtid = 0;
            header.length = sizeof header + hdlcFrame.size();
            header.status = 0;

            uint8_t* headerPtr = reinterpret_cast<uint8_t*>(&header);
            std::vector<uint8_t> netioFrame (sizeof header + hdlcFrame.size());
            std::copy(headerPtr, headerPtr+sizeof header, netioFrame.begin());
            std::copy(hdlcFrame.begin(), hdlcFrame.end(), netioFrame.begin()+sizeof header);

            netio::message msg( &netioFrame[0], netioFrame.size() );
            reply_socket.publish( publish_tag, msg );
            LOG(Log::TRC) << "Published reply( transaction id " << (int)payload[0] << ", sz=" << (int)netioFrame.size() << " )";

        }
	);
	simulator.subscribeReceiver( receiveCallback );

	netio::low_latency_recv_socket socket(
	        &context,
	        config.requestsPort,
	        [&simulator,&config,&generator,&distribution](netio::endpoint &ep, netio::message& msg)
        {
            LOG(Log::TRC) << "Received netio message, size=" << msg.size() << ", number of fragments: " << msg.num_fragments();
            if (msg.size() < 24) // TODO?
            {
                LOG(Log::INF) << "Obtained message smaller than expected for SCA, ignoring it. (It might be a HDLC control frame)";
                return;
            }
            if (simulateElinkDisruption)
            {
                LOG(Log::INF) << "Simulating disrupted elink connection - not passing the traffic to SCA";
                return;
            }

            std::vector<uint8_t> netioFrame (msg.data_copy());

            std::vector<uint8_t>::iterator current = netioFrame.begin();
            /* Step 1 - extract the header */
            size_t left = 0;
            while ( (left = std::distance( current, netioFrame.end())) > 0 )
            {
                LOG(Log::TRC) << "left=" << left;
                if (left < sizeof(felix::base::ToFELIXHeader))
                {
                    LOG(Log::ERR) << "Malformed data? Remaining buffer is smaller than ToFLX header.";
                    return;
                }
                felix::base::ToFELIXHeader* header = reinterpret_cast<felix::base::ToFELIXHeader*> (&*current);
                LOG(Log::TRC) << "header.len=" << header->length;


                if (header->elinkid == config.elinkId)
                {
                    auto fullHdlcFramePtr = current + sizeof(felix::base::ToFELIXHeader);
                    bool allZeros = std::all_of(
                            fullHdlcFramePtr,
                            fullHdlcFramePtr + header->length,
                            [](uint8_t x){return x==0;});
                    if (allZeros)
                    {
                        LOG(Log::TRC) << "Skipping zero-block of size " << header->length;
                        usleep (header->length);
                    }
                    else
                    {
                        bool acceptRequest = true;
                        if (config.doLoseReplies)
                        {
                            double randomNumber = distribution(generator);
                            if (randomNumber < config.requestsLossProbability)
                            {
                                acceptRequest = false;
                                LOG(Log::DBG) << "Losing a request! (simulated, selected by --request_loss)";
                            }
                        }
                        if (acceptRequest)
                        {
                            uint8_t* hdlcPayloadPtr = &*current + sizeof(felix::base::ToFELIXHeader) + HDLC_FRAME_PAYLOAD_OFFSET;
                            Hdlc::Payload payload(
                                    hdlcPayloadPtr,
                                    header->length - HDLC_FRAME_PAYLOAD_OFFSET - HDLC_FRAME_TRAILER_SIZE);
                            LOG(Log::TRC) << "obtained frame: " << payload.toString();
                            simulator.send( payload );
                        }
                    }
                }
                else
                    LOG(Log::ERR) << "Received a message destined for elink " << header->elinkid << ", ignoring it, ours is " << (unsigned int)config.elinkId;
                current += sizeof (*header) + header->length;
            }
        }
	);

	LOG(Log::INF) << "I think that I'm ready to accept connections from SCA-SW through netio. "
	        "I listen on port " << config.requestsPort << " for SCA requests "
	        "and I publish replies on port " << config.repliesPort <<
	        " and my elink is: " << (unsigned int)config.elinkId;
	while(1)
	{
		usleep(100000);
	}

}

